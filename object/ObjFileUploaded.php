<?php

class ObjFileUploaded extends \ObjProperties
{
    public $mode = 0777;

    protected $_file;

    protected $_error;

    public function __construct($file)
    {
        if (\HelperArray::is($file) and array_keys($file) == ['name', 'type', 'tmp_name', 'error', 'size'])
            $this->_file = $file;
    }

    public function move($destDir, $newName = null, $replaceNew = true)
    {
        $this->_error = null;

        if (!$this->_file) {
            $this->_error = "Undefined file";
            return false;
        }

        $file = $this->_file;
        if (!$newName)
            $newName = $file['name'];

        if (!\HelperDir::make($destDir, $this->mode, true, false)) {
            $this->_error = "Cannot create destination directory";
            return false;
        }

        $destFile = $destDir . $newName;
        if (!$replaceNew and \HelperFile::exists($destFile)) {
            $this->_error = "File already exists";
            return false;
        }

        $destFile = \HelperFile::path($destFile);
        if (!move_uploaded_file($this->_file['tmp_name'], $destFile)) {
            $this->_error = "Cannot move uploaded file";
            return false;
        }

        \HelperPath::mode($destFile, $this->mode);

        return true;
    }

    public function hasError()
    {
        return (boolean) $this->getError();
    }

    public function getError()
    {
        return $this->_error;
    }
}

