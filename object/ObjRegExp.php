<?php

class ObjRegExp extends \ObjProperties
{
    public $pattern;
    public $modifiers;
    public $delimiters;

    protected $_modifiers = [
            'i', // PCRE_CASELESS
            'm', // PCRE_MULTILINE
            's', // PCRE_DOTALL
            'x', // PCRE_EXTENDED
            // 'e', // PREG_REPLACE_EVAL - deprecated
            'A', // PCRE_ANCHORED
            'D', // PCRE_DOLLAR_ENDONLY
            'S', // pre analize
            'U', // PCRE_UNGREEDY
            'X', // PCRE_EXTRA
            'j', // PCRE_INFO_JCHANGED
            'u', // PCRE_UTF8
        ];
    protected $_delimiters = [
            '~',
            '/',
            '|',
            '!',
            '%',
            '#',
        ];

    /*
        @param string $pattern - regular expression, for example, '^.+$'
        @param string $modifiers - modifiers of regular expression, for example, 'isU'
        @params string $delimiters - delimiters of regular expression, for example, '/' or '~' etc, default - auto
    */
    public function __construct($pattern, $modifiers = '', $delimiters = null)
    {
        parent::__construct();

        $this->pattern = (string) $pattern;
        if (!$this->pattern)
            throw new \ExceptionProperty('Setting wrong value for property: ' . get_class($this) . '::pattern');

        if ($delimiters)
            $this->delimiters = \HelperString::sub((string) $delimiters, 0, 1);
        else {
            /* try to set auto */
            foreach ($this->_delimiters as $d) {
                if (\HelperString::pos($this->pattern, $d) === false) {
                    $this->delimiters = $d;
                    break;
                }
            }

        }
        if (!$this->delimiters)
            throw new \ExceptionProperty('Setting wrong value for property: ' . get_class($this) . '::delimiters');

        $this->modifiers = \HelperRegExp::replace('~[^' . implode($this->_modifiers) . ']~', '', (string) $modifiers);
    }

    public function toString()
    {
        return $this->delimiters . $this->pattern . $this->delimiters . $this->modifiers;
    }
}