<?php

class ObjUrlParsed extends \ObjProperties
{
    public $url;

    public $scheme;
    public $host;
    public $port;
    public $user;
    public $pass;
    public $path;
    public $query;
    public $hash;

    /*
        @param string $url
    */
    public function __construct($url) {
        parent::__construct();

        $this->url      = (string) $url;
        $parsed         = parse_url($this->url);

        $this->scheme   = !empty($parsed['scheme']) ? $parsed['scheme'] : null;
        $this->host     = !empty($parsed['host']) ? $parsed['host'] : null;
        $this->port     = !empty($parsed['port']) ? $parsed['port'] : null;
        $this->user     = !empty($parsed['user']) ? $parsed['user'] : null;
        $this->pass     = !empty($parsed['pass']) ? $parsed['pass'] : null;
        $this->path     = !empty($parsed['path']) ? $parsed['path'] : null;
        $this->query    = !empty($parsed['query']) ? $parsed['query'] : [];
        if ($this->query) {
            $queryArr = [];
            parse_str($this->query, $queryArr);
            $this->query = $queryArr;
        }
        $this->hash      = !empty($parsed['fragment']) ? $parsed['fragment'] : null;
    }
}