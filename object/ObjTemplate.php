<?php

class ObjTemplate extends \Obj
{
    public function __construct($params = null)
    {
        foreach ((array) $params as $param => $value) {
            if (!\HelperString::is($param))
                throw new \ExceptionProperty("Invalid parameter name '$param'");

            $this->$param = $value;
        }

        parent::__construct();
    }

    public function partial($view)
    {
        $path = APP_PATH . 'view/' . (string) $view . '.php';

        return $this->partialPath($path);
    }

    public function partialLib($view)
    {
        $path = SEA_PATH . 'view/' . (string) $view . '.php';

        return $this->partialPath($path);
    }

    public function partialPath($path)
    {
        $path = \HelperFile::path((string) $path);
        if (!\HelperFile::is($path))
            throw new \ExceptionFile("Undefined partial file '$path'");

        $this->__template = $path;

        return $this->partialTemplate();
    }

    protected $__template;

    protected function partialTemplate()
    {
        ob_start();
        require $this->__template;
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}