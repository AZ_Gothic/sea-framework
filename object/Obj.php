<?php

class Obj
{
    public function __construct() {}

    public function __get($name) {
        if (!isset($this->$name))
            return null;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __isset($name) {
        return property_exists($this, $name);
    }

    public function __unset($name) {
        $this->$name = null;
    }

    public function __call($name, $params) {
        throw new \ExceptionMethod('Calling unknown method: ' . get_class($this) . "::$name()");
    }

    public static function __callStatic($name, $params) {
        throw new \ExceptionMethod('Calling unknown static method: ' . get_called_class() . "::$name()");
    }
}