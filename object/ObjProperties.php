<?php

class ObjProperties extends \Obj
{
    public function __set($name, $value) {
        if (!isset($this->$name))
            throw new \ExceptionProperty('Setting unknown property: ' . get_class($this) . "::$name");
    }

    public function __get($name) {
        if (!isset($this->$name))
            throw new \ExceptionProperty('Getting unknown property: ' . get_class($this) . "::$name");
    }
}