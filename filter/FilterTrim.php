<?php

class FilterTrim implements \InterfaceFilter
{
    public static function filter($value, $params = null)
    {
        return trim(\HelperString::to($value));
    }
}