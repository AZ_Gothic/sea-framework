<?php

class FilterString implements \InterfaceFilter
{
    public static function filter($value, $params = null)
    {
        return \FilterType::filter($value, 'string');
    }
}