<?php

class FilterEmpty implements \InterfaceFilter
{
    public static function filter($value, $params = null)
    {
        return (empty($value) ? '' : $value);
    }
}