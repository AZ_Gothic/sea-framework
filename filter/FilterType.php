<?php

class FilterType implements \InterfaceFilter
{
    public static function filter($value, $params = null)
    {
        switch ((string) $params) {
            case 'str':
            case 'string':
                return (string) $value;
            case 'int':
            case 'integer':
                return (int) $value;
            case 'bool':
            case 'boolean':
                return (bool) $value;
            case 'float':
                return (float) $value;
            case 'array':
                return (array) $value;
        }

        return $value;
    }
}