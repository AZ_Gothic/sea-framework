<?php

class FilterToUpper implements \InterfaceFilter
{
    public static function filter($value, $params = null)
    {
        return \HelperString::toUpper($value);
    }
}