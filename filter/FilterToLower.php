<?php

class FilterToLower implements \InterfaceFilter
{
    public static function filter($value, $params = null)
    {
        return \HelperString::toLower($value);
    }
}