<?php

namespace sea;

class Loader extends Base
{
    public static $loader;

    public static function preLoader($className)
    {
        $pathPart = null;

        /* Exceptions */
        if (strncmp($className, 'Exception', 9) === 0)
            $pathPart = 'exception';
        /* Helpers */
        elseif (strncmp($className, 'Helper', 6) === 0)
            $pathPart = 'helper';
        /* Objects */
        elseif (strncmp($className, 'Obj', 3) === 0)
            $pathPart = 'object';
        /* Extensions */
        elseif (strncmp($className, 'Extensions', 10) !== 0 and strncmp($className, 'Extension', 9) === 0)
            $pathPart = 'extension';
        /* Interfaces */
        elseif (strncmp($className, 'Interface', 9) === 0)
            $pathPart = 'interface';
        /* Validator */
        elseif (strncmp($className, 'Validator', 9) === 0)
            $pathPart = 'validator';
        /* Filter */
        elseif (strncmp($className, 'Filter', 6) === 0)
            $pathPart = 'filter';

        if ($pathPart !== null)
            static::$loader->addClassMap([$className => SEA_PATH . $pathPart . DS . $className . '.php']);
    }

    public static $throw = true;

    public static function postLoader($className)
    {
        if (static::$throw)
            throw new \ExceptionClass("Cannot load class '$className'");
    }
}