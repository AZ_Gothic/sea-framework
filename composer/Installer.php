<?php

namespace sea\composer;

use Composer\Script\Event;

class Installer
{
    protected static $_event = null;

    /* composer.json

        "scripts": {
            "post-create-project-cmd": [
                "sea\\composer\\Installer::init"
            ]
        },
        "extra": {
            "sea\\composer\\Installer::init": {
                "reporting": false,
                "copyApp": true,
                "permissions": "0777"
            }
        }

    */
    protected static $_reporting    = true;
    protected static $_copyApp      = false;
    protected static $_permissions  = 0777;

    public static function init(Event $event)
    {
        echo "> Installing...\n";

        static::setEvent($event);
        static::setConstants();
        static::setParams(__METHOD__);
        static::setReporting();
        static::createApp();
    }

    protected static function setEvent(Event $event)
    {
        static::$_event = $event;
    }

    protected static function setConstants()
    {
        defined('DS') || define('DS', DIRECTORY_SEPARATOR);

        defined('APP_PATH') ||
            define('APP_PATH', dirname(dirname(dirname(dirname(dirname(__FILE__))))) . DS);

        defined('VENDOR_PATH') ||
            define('VENDOR_PATH', APP_PATH . 'vendor' . DS);
        defined('AZGOTHIC_PATH') ||
            define('AZGOTHIC_PATH', VENDOR_PATH . 'az_gothic' . DS);
        defined('SEA_PATH') ||
            define('SEA_PATH', AZGOTHIC_PATH . 'sea' . DS);
    }

    protected static function setParams($key)
    {
        $key = ltrim($key, '\\');

        $extras = static::$_event->getComposer()->getPackage()->getExtra();

        if (!empty($extras[$key]) and is_array($extras[$key])) {
            if (isset($extras[$key]['reporting']))
                static::$_reporting = (bool) $extras[$key]['reporting'];
            if (isset($extras[$key]['copyApp']))
                static::$_copyApp = (bool) $extras[$key]['copyApp'];
            if (!empty($extras[$key]['permissions']))
                static::$_permissions = octdec($extras[$key]['permissions']);
        }
    }

    protected static function setReporting()
    {
        echo "> Error Reporting: " . (int) static::$_reporting . "\n";

        if (static::$_reporting) {
            error_reporting(E_ALL);
            ini_set('display_startup_errors', 1);
            ini_set('display_errors', 1);
        }
        else {
            error_reporting(0);
            ini_set('display_startup_errors', 0);
            ini_set('display_errors', 0);
        }
    }

    protected static function createApp()
    {
        echo "> Copy Application schema: " . (int) static::$_copyApp . "\n";

        if (!static::$_copyApp) {
            echo "\n>>> SEA Framework uploaded!\n";
            return;
        }

        echo "> Copying...\n";

        $from = SEA_PATH . 'composer' . DS . 'app' . DS;
        $to = APP_PATH;

        $app = json_decode(file_get_contents($from . 'app.json'), true);

        foreach ($app['dir'] as $dir) {
            @mkdir($to . $dir, static::$_permissions, true);
            @chmod($to . $dir, static::$_permissions);
        }

        foreach ($app['file'] as $file) {
            @copy($from . $file, $to . $file);
            @chmod($to . $file, static::$_permissions);
        }

        @rename(APP_PATH . '.gitignore-example', APP_PATH . '.gitignore');
        @chmod($to . 'sea', 0766);
        @chmod($to . 'sea.bat', 0766);

        echo "\n>>> Your Application is ready!\n";
    }
}