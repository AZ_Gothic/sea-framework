<?php

namespace app\command;

class CommandIndex extends CommandBase
{
    public function actionIndex()
    {
        echo \HelperJson::encode(['Status' => 'OK']);
    }
}