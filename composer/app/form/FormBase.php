<?php

namespace app\form;

class FormBase extends \sea\Form
{
    public function formMethod()
    {
        return static::METHOD_POST;
    }
}