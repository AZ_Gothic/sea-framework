<!DOCTYPE html>
<HTML lang="en">
<HEAD>

    <title><?= $this->__title ?></title>

    <?= $this->__metas ?>
    <?= $this->__links ?>
    <?= $this->__styles ?>

</HEAD>

<BODY>

    <h1><?= $this->__title ?></h1>

    <div id="content">
        <?= $this->__content ?> 
    </div>

    <?= $this->__scripts ?>

</BODY>
</HTML>