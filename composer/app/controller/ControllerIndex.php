<?php

namespace app\controller;

class ControllerIndex extends ControllerBase
{
    public function actionIndex()
    {
        $this->view->title = 'Index Page';
    }
}