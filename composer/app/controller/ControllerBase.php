<?php

namespace app\controller;

class ControllerBase extends \sea\Controller
{
    public function beforeAction()
    {
        parent::beforeAction();

        /* set all AJAX Requests type of Response JSON */
        if (\Sea::$app->request->isAjax()) {
            if (\Sea::$app->request->isAjaxForm())
                $this->view->contentType = \View::CONTENT_TYPE_AJAXFORM;
            else
                $this->view->contentType = \View::CONTENT_TYPE_JSON;
        }

        \Sea::$app->view
            // ->registerMeta(['name' => 'description', 'content' => 'Some Description'], 'description')
            // ->registerMeta(['name' => 'keywords', 'content' => 'Some Keywords'], 'keywords')

            ->registerMeta(['http-equiv' => 'Content-Type', 'content' => 'text/html; charset=UTF-8'], 'Content-Type')
            ->registerMeta(['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge'], 'X-UA-Compatible')
            ->registerMeta(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'], 'viewport')

            // ->registerLink(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/favicon.ico'], 'icon')
            // ->registerLink(['rel' => 'shortcut icon', 'type' => 'image/x-icon', 'href' => '/favicon.ico'], 'shortcut icon')

            ->registerCss(APP_PATH . 'view/css/app.css', ['_priority' => 10])
            ->registerJs(APP_PATH . 'view/js/app.js', ['_priority' => 10])
        ;

    }
}