<?php

return [
    'extensions' => [
        'router' => [
            'params'    => [
                'redirectToCorrect' => true,
                'prettyUrls'        => true,
            ],
        ],
        'view' => [
            'params'    => [
                'title' => 'So Easy Application',
            ],
        ],
    ],
];