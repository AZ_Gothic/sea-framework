<?php

defined('APP_TYPE') || define('APP_TYPE', 'cli');

if (APP_TYPE == 'cli') {
    require_once __DIR__ . '/vendor/az_gothic/sea/AppCli.php';
    $app = new \sea\AppCli();
}
else {
    require_once __DIR__ . '/vendor/az_gothic/sea/App.php';
    $app = new \sea\App();
}

$config = \HelperArray::merge(
    require_once \HelperFile::path(CONFIG_PATH . 'main.php', true),
    require_once \HelperFile::path(CONFIG_PATH . 'main-local.php', true),
    require_once \HelperFile::path(CONFIG_PATH . APP_TYPE . '.php', true),
    require_once \HelperFile::path(CONFIG_PATH . APP_TYPE . '-local.php', true)
);

new \Sea($app, $config);