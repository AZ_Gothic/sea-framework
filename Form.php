<?php

namespace sea;

class Form extends Base
{
    const METHOD_POST = 'POST'; // default
    const METHOD_GET  = 'GET';

    protected $_fieldTypes = [
            'text'      => '\\sea\\form\\InputText',
            'password'  => '\\sea\\form\\InputPassword',
            'hidden'    => '\\sea\\form\\InputHidden',
            'file'      => '\\sea\\form\\InputFile',
            'checkbox'  => '\\sea\\form\\InputCheckbox',
            'radio'     => '\\sea\\form\\InputRadio',
            'submit'    => '\\sea\\form\\InputSubmit',
            'select'    => '\\sea\\form\\Select',
            'textarea'  => '\\sea\\form\\Textarea',
        ];
    protected $_formAttrs       = [];
    protected $_formFields      = null;
    protected $_formValues      = [];
    protected $_fieldsInstances = [];
    protected $_formErrors      = [];

    protected function formName()
    {
        return 'Form';
    }

    public function getFormName()
    {
        return $this->formName();
    }

    protected function formMethod()
    {
        return static::METHOD_POST;
    }

    public function getFormMethod()
    {
        return $this->formMethod();
    }

    protected function formFields()
    {
        return [];
    }

    public function getFormFields()
    {
        if ($this->_formFields !== null)
            return $this->_formFields;

        $fields = (array) $this->formFields();
        $formatFields = [];
        foreach ($fields as $k => $field)
            $formatFields = \HelperArray::merge($formatFields, $this->formatField($k, $field));

        $this->_formFields = $formatFields;

        return $formatFields;
    }

    public function init($presetValues = [])
    {
        $method = $this->formMethod;
        $data   = \Sea::$app->request->$method($this->formName, []);
        $files  = \Sea::$app->request->files($this->formName, []);

        foreach ($this->formFields as $name => $field) {
            if ($field['type'] == 'file') {
                if (isset($files[$name]))
                    $data[$name] = $files[$name];
            }
            $value = $field['value'];
            if (isset($data[$name]))
                $value = $data[$name];
            elseif (isset($presetValues[$name]))
                $value = $presetValues[$name];

            foreach ($field['filters'] as $filter) {
                $filterClass = $filter['class'];
                $value = $filterClass::filter($value, $filter['params']);
            }

            $this->field($name, $value);
        }
    }

    public function field($name, $presetValue = null)
    {
        if (!isset($this->_fieldsInstances[$name])) {
            if (!isset($this->_formFields[$name]))
                throw new \ExceptionProperty("Unknown field '$name'");

            $field  = $this->_formFields[$name];
            $class  = $this->_fieldTypes[$field['type']];

            $this->_fieldsInstances[$name] = new $class($field, $presetValue);
        }

        return $this->_fieldsInstances[$name];
    }

    public function validate()
    {
        $this->_formErrors = [];

        foreach ($this->formFields as $name => $field) {
            $res = $this->field($name)->validate();
            if (!$res->result)
                $this->_formErrors[$name] = $res->message;
        }

        return !$this->hasErrors();
    }

    public function hasErrors()
    {
        return !empty($this->_formErrors);
    }

    public function getErrors()
    {
        return $this->_formErrors;
    }

    public function begin($attrs = [])
    {
        $attrs = (array) $attrs;
        if (empty($attrs['action']))
            $attrs['action'] = null;
        $attrs['action'] = \HelperUrl::toAbsolute($attrs['action']);
        if (!empty($attrs['ajax'])) {
            $attrs['data-ajax']         = true;
            $attrs['data-ajax-action']  = !empty($attrs['ajaxAction']) ? $attrs['ajaxAction'] : $attrs['action'];
        }
        $attrs['data-ajax-action'] = \HelperUrl::toAbsolute($attrs['data-ajax-action']);

        unset($attrs['ajax'], $attrs['ajaxAction']);

        $attrs = \HelperArray::merge($attrs, ['id' => $this->formName, 'method' => $this->formMethod]);

        $res = '<FORM';
        foreach ($attrs as $attr => $val) {
            if (!$attr or \HelperArray::is($val) or \HelperObject::is($val))
                continue;

            $res .= ' ' . $attr . '="' . \HelperString::toAttr($val) . '"';
        }
        $res .= '>';

        return $res;
    }

    public function end()
    {
        return '</FORM>';
    }

    protected function formatField($fieldName, $fieldData)
    {
        if (is_string($fieldData)) {
            $fieldName = $fieldData;
            $fieldData = [];
        }

        if (!empty($fieldData['name']))
            $fieldName = $fieldData['name'];

        $type       = $this->formatFieldType((!empty($fieldData['type']) ? $fieldData['type'] : null));
        $required   = (int) (!empty($fieldData['required']) ? $fieldData['required'] : 0);
        $filters    = !empty($fieldData['filters']) ? $fieldData['filters'] : [];

        $validators = $this->formatFieldValidators((!empty($fieldData['validators']) ? $fieldData['validators'] : []));
        if ($required) {
            $notEmptyExists = false;
            foreach ($validators as $validator => $data) {
                if ($validator != 'notEmpty')
                    continue;
                $notEmptyExists = true;
                break;
            }
            if (!$notEmptyExists)
                $validators = $this->formatFieldValidators(\HelperArray::merge(['notEmpty'], $validators));
        }

        $formatData = [
            'type'      => $type,
            'name'      => $this->formName . '[' . $fieldName . ']',
            'value'     => (isset($fieldData['value']) ? $fieldData['value'] : null), // default value
            'id'        => (isset($fieldData['id']) ? $fieldData['id'] : $this->formName . '-' . $fieldName),

            'filters'    => $this->formatFieldFilters($filters),
            'validators' => $validators,
        ];

        if ($required)
            $formatData['required'] = true;
        unset($fieldData['required']);

        foreach ($fieldData as $k => $v) {
            if (!isset($formatData[$k]))
                $formatData[$k] = $v;
        }

        return [$fieldName => $formatData];
    }

    protected function formatFieldType($fieldType)
    {
        if (!$fieldType or !array_key_exists($fieldType, $this->_fieldTypes))
            $fieldType = 'inputText';

        return $fieldType;
    }

    protected function formatFieldFilters($filters)
    {
        $formatFilters = [];
        foreach ($filters as $name => $data) {
            if (!\HelperString::is($name) and \HelperString::is($data))
                $name = $data;

            if (\HelperString::is($data))
                $data = [];

            if (empty($data['class']))
                $data['class'] = '\\Filter' . \HelperString::toUpperFirst($name);

            $formatFilters[$name] = [
                'class'  => $data['class'],
                'params' => (!empty($data['params']) ? $data['params'] : null),
            ];
        }

        return $formatFilters;
    }

    protected function formatFieldValidators($validators)
    {
        $formatValidators = [];
        foreach ($validators as $name => $data) {
            if (!\HelperString::is($name) and \HelperString::is($data))
                $name = $data;

            if (\HelperString::is($data))
                $data = [];

            if (empty($data['class']))
                $data['class'] = '\\Validator' . \HelperString::toUpperFirst($name);

            $formatValidators[$name] = [
                'class'   => $data['class'],
                'params'  => (!empty($data['params']) ? $data['params'] : null),
                'message' => (!empty($data['message']) ? $data['message'] : null),
            ];
        }

        return $formatValidators;
    }
}