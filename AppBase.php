<?php

namespace sea;

require_once 'Base.php';

class AppBase extends Base
{
    public function init()
    {
        defined('DS') || define('DS', DIRECTORY_SEPARATOR);

        /* Pathes */
        defined('APP_PATH') ||
            define('APP_PATH', dirname(dirname(dirname(dirname(__FILE__)))) . DS);

        defined('VENDOR_PATH') ||
            define('VENDOR_PATH', APP_PATH . 'vendor' . DS);
        defined('AZGOTHIC_PATH') ||
            define('AZGOTHIC_PATH', VENDOR_PATH . 'az_gothic' . DS);
        defined('SEA_PATH') ||
            define('SEA_PATH', AZGOTHIC_PATH . 'sea' . DS);

        defined('CONFIG_PATH') ||
            define('CONFIG_PATH', APP_PATH . 'config' . DS);
        defined('RUNTIME_PATH') ||
            define('RUNTIME_PATH', APP_PATH . 'runtime' . DS);
        defined('LOG_PATH') ||
            define('LOG_PATH', RUNTIME_PATH . 'logs' . DS);

        defined('WEB_PATH') ||
            define('WEB_PATH', APP_PATH . 'web' . DS);
        defined('VERSIONS_PATH') ||
            define('VERSIONS_PATH', WEB_PATH . 'versions' . DS);
        defined('CSS_PATH') ||
            define('CSS_PATH', WEB_PATH . 'css' . DS);
        defined('JS_PATH') ||
            define('JS_PATH', WEB_PATH . 'js' . DS);

        defined('WWW_PATH') ||
            define('WWW_PATH', '/');
        defined('VERSIONS_WWW_PATH') ||
            define('VERSIONS_WWW_PATH', WWW_PATH . 'versions/');
        defined('CSS_WWW_PATH') ||
            define('CSS_WWW_PATH', WWW_PATH . 'css/');
        defined('JS_WWW_PATH') ||
            define('JS_WWW_PATH', WWW_PATH . 'js/');

        /* Loader */
        require_once SEA_PATH . 'Loader.php';

        if (!$this->getLoader()) {
            \sea\Loader::$loader = require VENDOR_PATH . 'autoload.php';

            $this->getLoader()->addClassMap(['Sea' => SEA_PATH . 'Sea.php']);

            $this->getLoader()->addPsr4('app\\', APP_PATH);
            $this->getLoader()->addPsr4('sea\\', SEA_PATH);

            $this->getLoader()->addPsr4('', SEA_PATH . 'extension');

            spl_autoload_register(['\\sea\\Loader', 'preLoader'], true, true);
            spl_autoload_register(['\\sea\\Loader', 'postLoader'], true, false);

            set_error_handler(['\\ExceptionHandler', 'error'], E_ALL);
        }
    }

    public function begin($config)
    {
        ob_start();

        if (!\HelperArray::is($config))
            throw new \ExceptionApp('Wrong config format');

        $extensions = !empty($config['extensions']) ? $config['extensions'] : [];
        unset($config['extensions']);

        $this->setExtensions();

        /* set default extensions */
        $extensions = \HelperArray::merge(Extensions::normalize($this->getDefaultExtensions($config)),
                                          Extensions::normalize($extensions));

        /* set config */
        $this->extensions->set('config', $extensions['config']);
        unset($extensions['config']);

        /* set env */
        defined('APP_ENV') ||
            define('APP_ENV', $this->config->get('env', 'dev'));

        /* set timezone */
        if ($timezone = $this->config->timezone)
            date_default_timezone_set($timezone);

        /* set extensions */
        foreach ((array) $extensions as $ext => $data)
            $this->extensions->set($ext, $data);

        /* continue */
        $this->content();
        $this->finish();
    }

    public $content = '';

    protected function content()
    {
        $this->content = \sea\Controller::runAction();

        return $this;
    }

    protected $_finished;

    public function finish($alreadySent = false)
    {
        if ($this->_finished)
            exit;

        $this->_finished = true;

        /* set env */
        defined('APP_ENV') ||
            define('APP_ENV', 'dev');

        $content = ob_get_contents();
        ob_end_clean();

        $debug = null;
        if ($alreadySent)
            $this->content = $content;
        else
            $debug = $content;

        /* finish extensions */
        $this->finishExtensions();

        if (APP_ENV != 'prod' and $debug) {
            if ($this->has('view') and $this->ext('view')->getContentType() == \View::CONTENT_TYPE_JSON) {
                $content = (array) \HelperJson::decode($this->content);
                $content = \HelperArray::merge(['DEBUG' => $debug], $content);
                $this->content = \HelperJson::encode($content);
            }
            elseif ($this->has('view') and $this->ext('view')->getContentType() == \View::CONTENT_TYPE_AJAXFORM)
                $this->content .= PHP_EOL . '<pre id="DEBUG">' . PHP_EOL . PHP_EOL . $debug . PHP_EOL . '</pre>' . PHP_EOL;
            else
                $this->content .= PHP_EOL . PHP_EOL . '#DEBUG:' . PHP_EOL . PHP_EOL . $debug . PHP_EOL;
        }

        /* echo content */
        echo $this->content;

        exit;
    }


    /********** EXTENSIONS **********/

    protected $extensions;

    protected function setExtensions()
    {
        $this->extensions = new Extensions();
    }

    public function __get($name)
    {
        if ($this->has($name))
            return $this->ext($name);

        return parent::__get($name);
    }

    public function ext($name)
    {
        return $this->extensions->get($name);
    }

    public function has($name)
    {
        return $this->extensions->has($name);
    }

    /********** end EXTENSIONS **********/

    public function getLoader()
    {
        return \sea\Loader::$loader;
    }

    protected function getDefaultExtensions($config)
    {
        return [
                'config'    => [
                    'class'  => '\\Config',
                    'params' => $config,
                ],
                'event'     => '\\Event',
                'logger'    => '\\Logger',
                'request'   => '\\Request',
                'response'  => '\\Response',
                'router'    => '\\Router',
                'view'      => '\\View',
            ];
    }

    protected function finishExtensions()
    {
        if ($extensions = $this->extensions->getAll()) {
            $extensions = array_reverse($extensions, true);
            foreach ($extensions as $ext)
                if (method_exists($ext, 'finish'))
                    $ext->finish();
        }
    }
}