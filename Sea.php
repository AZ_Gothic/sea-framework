<?php

class Sea extends \sea\Base
{
    /**
     * @property static \sea\App $app - instance of application class
     */
    public static $app;

    /**
     * @property static bool $__constructed - already constructed class with config
     */
    protected static $__constructed;

    public function init(\sea\AppBase $app, array $config)
    {
        if (static::$__constructed)
            throw new \ExceptionApp('Application already constructed');

        static::$__constructed = true;

        static::$app = $app;
        static::$app->begin($config);
    }
}