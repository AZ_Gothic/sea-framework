<?php

interface InterfaceValidator
{
    public static function validate($value, $params = null, $message = null);
}