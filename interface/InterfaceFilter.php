<?php

interface InterfaceFilter
{
    public static function filter($value, $params = null);
}