<?php

/*
    helper class for work with trace lines
*/
class HelperTrace
{
    public static function last()
    {
        $ts = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        array_pop($ts); // remove the last trace since it would be the entry script, not very useful
        foreach ($ts as $trace) {
            if (isset($trace['file'], $trace['line']) and strpos($trace['file'], AZGOTHIC_PATH) !== 0) {
                unset($trace['object'], $trace['args']);

                return $trace;
            }
        }

        return null;
    }
}