<?php

/*
    helper class for work with strings
*/
class HelperString
{
    const ENCODING = 'UTF-8';

    public static function is($str) {
        return is_string($str);
    }

    public static function to($var) {
        return (string) $var;
    }

    public static function toUpper($str) {
        if (!static::is($str))
            return $str;

        return mb_strtoupper($str, static::ENCODING);
    }

    public static function toLower($str) {
        if (!static::is($str))
            return $str;

        return mb_strtolower($str, static::ENCODING);
    }

    public static function toUpperFirst($str) {
        if (!static::is($str))
            return $str;

        return static::toUpper(static::sub($str, 0, 1)) . static::sub($str, 1);
    }

    public static function toLowerFirst($str) {
        if (!static::is($str))
            return $str;

        return static::toLower(static::sub($str, 0, 1)) . static::sub($str, 1);
    }

    public static function len($str) {
        return mb_strlen($str, static::ENCODING);
    }

    public static function pos($haystack, $needle, $offset = 0) {
        return mb_strpos($haystack, $needle, $offset, static::ENCODING);
    }

    public static function ipos($haystack, $needle, $offset = 0) {
        return mb_stripos($haystack, $needle, $offset, static::ENCODING);
    }

    public static function sub($str, $start, $length = null) {
        return mb_substr($str, $start, $length, static::ENCODING);
    }

    public static function replace($search, $replace, $str, $caseless = false) {
        if ($caseless)
            return str_ireplace($search, $replace, $str);
        return str_replace($search, $replace, $str);
    }

    public static function ireplace($search, $replace, $str) {
        return static::replace($search, $replace, $str, true);
    }

    public static function html($str) {
        if (!static::is($str))
            return $str;

        return htmlspecialchars($str, ENT_COMPAT | ENT_HTML401, static::ENCODING);
    }

    public static function html_decode($str) {
        if (!static::is($str))
            return $str;

        return htmlspecialchars_decode($str, ENT_COMPAT | ENT_HTML401);
    }

    public static function toAttr($str) {
        if (\HelperBoolean::is($str))
            return $str ? 'true' : 'false';

        return static::replace('"', '&quot;', $str);
    }

    public static function toUrl($str) {
        if (!static::is($str))
            return $str;

        return urlencode($str);
    }
}