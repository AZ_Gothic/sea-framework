<?php

/*
    helper class for work with integers
*/
class HelperInteger
{
    public static function is($int) {
        return is_int($int);
    }
}