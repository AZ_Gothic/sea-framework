<?php

/*
    helper class for work with urls
*/
class HelperUrl
{
    public static $argSeparator = '&';
    protected static $replaces = [
            '%2F' => '/',
            '%5B' => '[',
            '%5D' => ']',
        ];

    public static function to($UrlOrRoute = null, $params = null, $clearOldParams = false) {
        return \Sea::$app->router->to($UrlOrRoute, $params, $clearOldParams);
    }

    public static function toAbsolute($UrlOrRoute = null, $params = null, $clearOldParams = false) {
        return \Sea::$app->router->toAbsolute($UrlOrRoute, $params, $clearOldParams);
    }

    public static function parse($url, $prop = null) {
        if ($url instanceof \ObjUrlParsed)
            $res = $url;
        else
            $res = new \ObjUrlParsed($url);

        return !$prop ? $res : $res->$prop;
    }

    public static function getCurrent() {
        return \Sea::$app->router->get('current');
    }

    public static function getCurrentObj($prop = null) {
        return static::parse(static::getCurrent(), $prop);
    }

    public static function isAbsolute($url) {
        return (static::parse($url, 'host') !== null) ? true : false;
    }

    public static function constructUrl($url, $includePath = true) {
        if ($url instanceof \ObjUrlParsed)
            $parsed = $url;
        elseif (!$url or \HelperString::is($url))
            $parsed = static::parse($url);
        else
            return '';

        $res = '';
        if ($parsed->scheme)
            $res .= $parsed->scheme . '://';
        if ($parsed->host) {
            if ($parsed->scheme)
                $res .= $parsed->host;
            else
                $res .= '//' . $parsed->host;
            if ($parsed->port)
                $res .= ':' . $parsed->port;
        }
        if ($includePath and $parsed->path)
            $res .= $parsed->path;

        return $res;
    }

    public static function construct($url, $params = null) {
        return static::constructUrl($url, true) . ($params ? '?' . static::buildQuery($params) : '');
    }

    public static function buildQuery($data, $sep = null) {
        if (!$data)
            return '';
        if (!$sep)
            $sep = static::$argSeparator;
        return \HelperString::replace(array_keys(static::$replaces), static::$replaces, http_build_query($data, '', $sep));
    }

    public static function normalizeParams($data) {
        if (is_array($data))
            $data = static::buildQuery($data);

        $queryArr = [];
        parse_str((string) $data, $queryArr);

        return $queryArr;
    }
}