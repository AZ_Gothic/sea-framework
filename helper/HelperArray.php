<?php

/*
    helper class for work with arrays
*/
class HelperArray
{
    public static function is($array)
    {
        return is_array($array);
    }

    public static function in($needle, $haystack, $strict = false)
    {
        return in_array($needle, (array) $haystack, $strict);
    }

    public static function search($needle, $haystack, $strict = false)
    {
        return array_search($needle, (array) $haystack, $strict);
    }

    public static function keyExists($key, $haystack)
    {
        return array_key_exists($key, (array) $haystack);
    }

    public static function unique($array)
    {
        return array_unique((array) $array);
    }

    public static function merge() {
        $arrays = func_get_args();
        $res    = array_shift($arrays);
        while (!empty($arrays)) {
            $next = array_shift($arrays);
            foreach ($next as $k => $v) {
                if (\HelperInteger::is($k)) {
                    if (isset($res[$k]))
                        $res[] = $v;
                    else
                        $res[$k] = $v;
                }
                elseif (static::is($v) and isset($res[$k]) and static::is($res[$k]))
                    $res[$k] = static::merge($res[$k], $v);
                else
                    $res[$k] = $v;
            }
        }

        return $res;
    }
}