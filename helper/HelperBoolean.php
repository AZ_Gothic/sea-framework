<?php

/*
    helper class for work with booleans
*/
class HelperBoolean
{
    public static function is($bool) {
        return is_bool($bool);
    }
}