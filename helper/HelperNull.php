<?php

/*
    helper class for work with null
*/
class HelperNull
{
    public static function is($null) {
        return is_null($null);
    }
}