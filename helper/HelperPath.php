<?php

/*
    helper class for work with pathes
*/
class HelperPath
{
    public static function real($path)
    {
        $parts      = [];
        $path       = \HelperString::replace('\\', '/', $path);
        $path       = \HelperRegExp::replace(new \ObjRegExp('/+'), '/', $path);
        $segments   = explode('/', $path);
        $test       = '';

        foreach ($segments as $segment) {
            if($segment == '.')
                continue;

            $test = array_pop($parts);
            if ($test === null)
                $parts[] = $segment;
            elseif ($segment == '..') {
                if($test == '..')
                    $parts[] = $test;

                if ($test == '..' or $test == '')
                    $parts[] = $segment;
            }
            else {
                $parts[] = $test;
                $parts[] = $segment;
            }
        }

        return implode(DS, $parts);
    }

    public static function mode($path, $mode = 0777)
    {
        if (!\HelperFile::exists($path) and !\HelperDir::exists($path))
            return false;

        return @chmod(static::real($path), static::realMode($mode));
    }

    public static function realMode($mode)
    {
        return octdec(decoct($mode));
    }

    public static function move($old_name, $new_name, $mode = null, $throw = true)
    {
        if (!\HelperFile::exists($old_name) and !\HelperDir::exists($old_name)) {
            if ($throw)
                throw new \ExceptionFile("Undefined path '$old_name'");

            return false;
        }

        $dirNew = dirname($new_name);
        if (!\HelperDir::make($dirNew, $mode, true, $throw))
            return false;

        if (!rename($old_name, $new_name)) {
            if ($throw)
                throw new \ExceptionFile("Cannot move from '$old_name' to '$new_name'");

            return false;
        }

        if ($mode !== null)
            return static::mode($new_name, $mode);

        return true;
    }
}