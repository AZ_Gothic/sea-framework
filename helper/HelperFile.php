<?php

/*
    helper class for work with files
*/
class HelperFile
{
    public static function is($file)
    {
        return is_file(static::path($file));
    }

    public static function exists($file)
    {
        return file_exists(static::path($file)) and static::is($file);
    }

    public static function isReadable($file)
    {
        return file_exists(static::path($file)) and static::is($file) and is_readable($file);
    }

    public static function isWritable($file)
    {
        return file_exists(static::path($file)) and static::is($file) and is_writable($file);
    }

    public static function path($file, $throw = false)
    {
        if ($throw and !static::exists($file))
            throw new \ExceptionFile("Undefined file '$file'");

        return \HelperPath::real($file);
    }

    public static function modified($file, $format = 'Y-m-d H:i:s')
    {
        if (!static::exists($file))
            return 0;

        return date($format, filemtime($file));
    }

    public static function size($file)
    {
        if (!static::exists($file))
            throw new \ExceptionFile("Undefined file '$file'");

        return filesize(static::path($file));
    }

    public static function formatSize($file, $decimals = 2)
    {
        $bytes  = static::size($file);
        $sz     = 'BKMGTP';
        $factor = floor((\HelperString::len($bytes) - 1) / 3);

        return round($bytes / pow(1024, $factor), $decimals) . @$sz[$factor];
    }

    public static function put($file, $data, $append = true, $mode = 0777, $throw = true)
    {
        $dir = dirname($file);
        if (!\HelperDir::make($dir, $mode, true, $throw))
            return false;

        if (file_put_contents($file, (string) $data, ($append ? FILE_APPEND : 0)) === false) {
            if ($throw)
                throw new \ExceptionFile("Cannot write to file '$file'");

            return false;
        }

        \HelperPath::mode($file, $mode);

        return true;
    }

    public static function get($file, $throw = true)
    {
        if (!static::isReadable($file) or ($content = file_get_contents($file)) === false) {
            if ($throw)
                throw new \ExceptionFile("Cannot read file '$file'");

            return '';
        }

        return $content;
    }

    public static function copy($from, $to, $mode = 0777, $throw = true)
    {
        if (!static::exists($from)) {
            if ($throw)
                throw new \ExceptionFile("Undefined file '$from'");

            return false;
        }

        $dir = dirname($to);
        if (!\HelperDir::make($dir, $mode, true, $throw))
            return false;

        $from = static::path($from);
        $to   = static::path($to);

        if (!copy($from, $to)) {
            if ($throw)
                throw new \ExceptionFile("Cannot copy file '$from' to '$to'");

            return false;
        }

        if ($mode)
            return \HelperPath::mode($to, $mode);

        return true;
    }

    public static function remove($file)
    {
        if (static::exists($file))
            unlink($file);

        return !static::exists($file);
    }
}