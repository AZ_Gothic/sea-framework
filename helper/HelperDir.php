<?php

/*
    helper class for work with directories
*/
class HelperDir
{
    const TYPE_DIR      = 'dir';
    const TYPE_FILE     = 'file';
    const TYPE_ELSE     = 'else';

    const ONLY_ALL      = null;
    const ONLY_DIRS     = 'dirs';
    const ONLY_FILES    = 'files';

    public static function is($dir)
    {
        return is_dir(static::path($dir));
    }

    public static function exists($dir)
    {
        return file_exists(static::path($dir)) and static::is($dir);
    }

    public static function path($dir, $throw = false)
    {
        if ($throw and !static::exists($dir))
            throw new \ExceptionDir("Undefined directory '$dir'");

        return \HelperPath::real($dir) . DIRECTORY_SEPARATOR;
    }

    public static function modified($dir, $format = 'Y-m-d H:i:s')
    {
        if (!static::exists($dir))
            return 0;

        return date($format, filemtime(rtrim($dir, '\\/') . DS . '.'));
    }

    public static function make($dir, $mode = 0777, $recursive = true, $throw = true)
    {
        if (!static::exists($dir)) {
            if ($recursive) {
                $notExists = static::notExists($dir);
                foreach ($notExists as $notExist) {
                    if (!static::make($notExist, $mode, false, $throw))
                        return false;
                }
            }
            else {
                if (!mkdir($dir, \HelperPath::realMode($mode), false)) {
                    if ($throw)
                        throw new \ExceptionDir("Cannot make directory '$dir'");

                    return false;
                }
            }
        }

        \HelperPath::mode($dir, $mode);

        return true;
    }

    public static function notExists($dir)
    {
        $notExists = [];
        while (!static::exists($dir)) {
            $notExists[] = $dir;
            $dir = dirname($dir);
        }

        return array_reverse($notExists);
    }

    public static function ls($dir, $onlyType = null)
    {
        $list = [];

        if (!static::exists($dir))
            return $list;

        $list = [
                static::TYPE_DIR  => [],
                static::TYPE_FILE => [],
                static::TYPE_ELSE => [],
            ];

        $dr = @opendir($dir);
        while (false !== ($name = @readdir($dr))) {
            if ($name != '.' and $name != '..') {
                $path = \HelperPath::real($dir . DS . $name);
                $type = static::is($path) ? static::TYPE_DIR : (\HelperFile::is($path) ? static::TYPE_FILE : static::TYPE_ELSE);
                $list[$type][] = [
                        'type' => $type,
                        'name' => $name,
                        'path' => $path . ($type == static::TYPE_DIR ? DS : ''),
                    ];
            }
        }
        closedir($dr);

        if (empty($list[static::TYPE_DIR]))
            $list[static::TYPE_DIR] = [];
        if (empty($list[static::TYPE_FILE]))
            $list[static::TYPE_FILE] = [];

        if ($onlyType == static::ONLY_DIRS or $onlyType == static::TYPE_DIR)
            return $list[static::TYPE_DIR];
        if ($onlyType == static::ONLY_FILES or $onlyType == static::TYPE_FILE)
            return $list[static::TYPE_FILE];

        return \HelperArray::merge($list[static::TYPE_DIR], $list[static::TYPE_FILE], $list[static::TYPE_ELSE]);
    }

    public static function remove($dir)
    {
        if (!static::exists($dir))
            return true;

        $dir = trim($dir, '\\/') . DS;

        $list = static::ls($dir);
        foreach ($list as $item) {
            if (static::is($item['path'])) {
                if (!static::remove($item['path']))
                    return false;
            }
            else {
                if (!\HelperFile::remove($item['path']))
                    return false;
            }
        }

        return rmdir($dir);
    }

    public static function copy($src, $dest, $mode = 0777, $throw = true)
    {
        $list = static::ls($src);
        if (!static::make($dest, $mode, true, $throw))
            return false;

        if (!$list)
            return true;

        foreach ($list as $item) {
            if ($item['type'] == static::TYPE_DIR) {
                if (!static::copy($item['path'], $dest . DS . $item['name'], $mode, $throw))
                    return false;
            }
            else {
                if (!\HelperFile::copy($item['path'], $dest . DS . $item['name'], $mode, $throw))
                    return false;
            }
        }

        return true;
    }
}