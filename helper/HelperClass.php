<?php

/*
    helper class for work with classes
*/
class HelperClass
{
    public static function exists($class, $autoload = true)
    {
        $throwTmp = \sea\Loader::$throw;
        \sea\Loader::$throw = false;
        $exists = (class_exists($class, $autoload) or interface_exists($class, $autoload) or trait_exists($class, $autoload));
        \sea\Loader::$throw = $throwTmp;

        return $exists;
    }
}