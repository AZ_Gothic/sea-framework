<?php

/*
    helper class for work with Zip files
*/
class HelperZip
{
    public static function archive($path, $destFile, $mode = 0777, $throw = true)
    {
        $destFile = \HelperFile::path($destFile);
        $dest = \HelperDir::path(dirname($destFile));

        if (!\HelperDir::make($dest, $mode, true, $throw))
            return false;

        if (!\HelperDir::is($path) and !\HelperFile::is($path)) {
            if ($throw)
                throw new \ExceptionDir("Path '$path' not found");

            return false;
        }

        $zip = new \ZipArchive();
        if (!$zip->open($destFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
            if ($throw)
                throw new \ExceptionFile("Cannot create Zip file '$destFile'");

            return false;
        }

        if (\HelperFile::is($path)) {
            $root = \HelperDir::path(dirname($path));
            $list = [
                    'path'  => \HelperFile::path($path),
                    'rel'   => basename($path),
                ];
        }
        else {
            $root    = \HelperDir::path($path);
            $rootLen = \HelperString::len($root);
            $items   = new \RecursiveIteratorIterator(
                            new \RecursiveDirectoryIterator($root),
                            \RecursiveIteratorIterator::LEAVES_ONLY
                        );

            $list = [];
            foreach ($items as $item) {
                if (!$item->isDir()) {
                    $p = \HelperFile::path($item->getRealPath());
                    $list[] = [
                            'path'  => $p,
                            'rel'   => \HelperString::sub($p, $rootLen),
                        ];
                }
            }
        }

        foreach($list as $item) {
            if (!$zip->addFile($item['path'], $item['rel'])) {
                if ($throw)
                    throw new \ExceptionFile("Cannot add file '" . $item['path'] . "' with relative path '" . $item['rel'] . "' to Zip file '$destFile'");

                return false;
            }
        }

        if (!$zip->close()) {
            if ($throw)
                throw new \ExceptionFile("Cannot saev Zip file '$destFile'");

            return false;
        }

        if ($mode)
            return \HelperPath::mode($destFile, $mode);

        return true;
    }
}