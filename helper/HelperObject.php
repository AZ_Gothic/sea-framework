<?php

/*
    helper class for work with objects
*/
class HelperObject
{
    public static function is($object)
    {
        return is_object($object);
    }
}