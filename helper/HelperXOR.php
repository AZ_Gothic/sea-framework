<?php

/*
    helper class for work with XOR crypt
*/
class HelperXOR
{
    protected static $_check = 'XOR::';

    public static function encode($str, $key)
    {
        return static::$_check . base64_encode(static::recode($str, $key));
    }

    public static function decode($str, $key)
    {
        /* not encrypted or used another algorithm */
        if (strncmp($str, static::$_check, \HelperString::len(static::$_check)) !== 0)
            return $str;

        return static::recode(base64_decode(\HelperString::sub($str, (\HelperString::len(static::$_check) - 1))), $key);
    }

    protected static function recode($str, $key)
    {
        /* don't use MB_ functions! */
        $len    = strlen($str);
        $gamma  = '';
        $n      = $len > 100 ? 8 : 2;

        while (strlen($gamma) < $len)
            $gamma .= substr(pack('H*', sha1($key . $gamma)), 0, $n);

        return $str^$gamma;
    }
}