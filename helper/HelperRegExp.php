<?php

/*
    helper class for work with regexp functions
*/
class HelperRegExp
{
    public static function replace($pattern, $replacement, $subject, $limit = -1)
    {
        if ($pattern instanceof \ObjRegExp)
            $pattern = $pattern->toString();
        return preg_replace($pattern, $replacement, $subject, $limit);
    }

    /*
        $matches is array of found conditions in () with keys from 0, not from 1 as in preg_match
            - this logic has been added to be similar to the method matchAll
    */
    public static function match($pattern, $subject, &$matches = null, $offset = 0)
    {
        if ($pattern instanceof \ObjRegExp)
            $pattern = $pattern->toString();
        $res = preg_match($pattern, $subject, $matches, 0, $offset);
        unset($matches[0]);
        $matches = array_values((array) $matches);

        return $res;
    }

    /*
        $matches is array of found conditions in () with keys from 0, not double array as in preg_match_all
    */
    public static function matchAll($pattern, $subject, &$matches = null, $offset = 0)
    {
        if ($pattern instanceof \ObjRegExp)
            $pattern = $pattern->toString();
        $res     = preg_match_all($pattern, $subject, $matches, PREG_PATTERN_ORDER /* PREG_SET_ORDER*/, $offset);
        $matches = isset($matches[1]) ? $matches[1] : [];

        return $res;
    }

    /*
        return new \ObjRegExp
    */
    public static function obj($pattern, $modifiers = '', $delimiters = null)
    {
        return new \ObjRegExp($pattern, $modifiers, $delimiters);
    }
}