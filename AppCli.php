<?php

namespace sea;

require_once 'AppBase.php';

class AppCli extends AppBase
{
    protected function content()
    {
        \sea\Command::runCommandAction();

        return $this;
    }

    public function finish()
    {
        if ($this->_finished)
            exit;

        $this->_finished = true;

        /* set env */
        defined('APP_ENV') ||
            define('APP_ENV', 'dev');

        $this->content = ob_get_contents();
        ob_end_clean();

        /* finish extensions */
        $this->finishExtensions();

        /* echo content */
        echo $this->content;

        exit;
    }

    protected function getDefaultExtensions($config)
    {
        return [
                'config'    => [
                    'class'  => '\\Config',
                    'params' => $config,
                ],
                'event'     => '\\Event',
                'logger'    => '\\LoggerCli',
                'request'   => '\\RequestCli',
                // 'response'  => '\\ResponseCli',
                'router'    => '\\RouterCli',
                // 'view'      => '\\ViewCli',
            ];
    }
}