<?php

namespace sea;

class Command extends Controller
{
    public static function getControllerName($routeController)
    {
        return '\\app\\command\\Command' . \HelperString::toUpperFirst($routeController);
    }

    public function init()
    {
        /* do not run parent::init() */
    }

    public static function runCommandAction($controller = null, $action = null)
    {
        static::runAction($controller, $action, false);
    }
}