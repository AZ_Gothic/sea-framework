<?php

namespace sea\form;

class Input extends Type
{
    public function construct()
    {
        $field = $this->get();
        $field['type'] = 'text';

        return $this->constructField($field);
    }

    public function constructField($field)
    {
        return \Sea::$app->view->partialLib('form/input', [
                'form' => [
                    'field'     => $field,
                    'value'     => $this->getValue(),
                    'hasError'  => $this->hasError(),
                    'error'     => $this->getError(),
                ]
            ]);
    }
}