<?php

namespace sea\form;

class InputFile extends Input
{
    public function construct()
    {
        return \Sea::$app->view->partialLib('form/file', [
                'form' => [
                    'field'     => $this->get(),
                    'hasError'  => $this->hasError(),
                    'error'     => $this->getError(),
                ]
            ]);
    }
}