<?php

namespace sea\form;

class InputPassword extends Input
{
    public function construct()
    {
        $field = $this->get();
        $field['type'] = 'password';

        return $this->constructField($field);
    }
}