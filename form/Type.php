<?php

namespace sea\form;

abstract class Type extends \sea\Base
{
    protected $_value;
    protected $_filters;
    protected $_validators;
    protected $_field;
    protected $_error;

    public function __construct($field, $value = null)
    {
        unset($field['value']);
        $this->setValue($value);
        $this->_filters    = $field['filters'];
        $this->_validators = $field['validators'];
        unset($field['filters'], $field['validators']);
        $this->_field = $field;
    }

    public function setValue($value)
    {
        $this->_value = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function get($prop = null, $default = null)
    {
        $field = $this->_field;
        if (!$prop)
            return $field;
        return isset($field[$prop]) ? $field[$prop] : $default;
    }

    public function getFilters()
    {
        return $this->_filters;
    }

    public function getValidators()
    {
        return $this->_validators;
    }

    public function hasError()
    {
        return $this->_error !== null;
    }

    public function getError()
    {
        return $this->_error;
    }

    abstract public function construct();

    public function validate()
    {
        $this->_error = null;

        $validators = $this->getValidators();
        foreach ($validators as $validator) {
            $validatorClass = $validator['class'];
            $res = $validatorClass::validate($this->getValue(), $validator['params'], $validator['message']);
            if (!$res->result) {
                $this->_error = $res->message;

                return $res;
            }
        }

        return new \ObjValidate;
    }
}