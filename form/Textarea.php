<?php

namespace sea\form;

class Textarea extends Type
{
    public function construct()
    {
        return \Sea::$app->view->partialLib('form/textarea', [
                'form' => [
                    'field'     => $this->get(),
                    'value'     => $this->getValue(),
                    'hasError'  => $this->hasError(),
                    'error'     => $this->getError(),
                ]
            ]);
    }
}