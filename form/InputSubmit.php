<?php

namespace sea\form;

class InputSubmit extends Input
{
    public function construct()
    {
        $field = $this->get();
        $field['type'] = 'submit';
        unset($field['name']);

        return $this->constructField($field);
    }
}