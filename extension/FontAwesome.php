<?php

class FontAwesome extends \Extension
{
    protected $version = '4.7.0';

    public function __construct($params = [])
    {
        parent::__construct($params);

        $pluginName = 'font-awesome-' . $this->version;
        $pluginSrc  = SEA_PATH . 'extension' . DS . 'FontAwesome' . DS . $pluginName . DS;

        \Sea::$app->view
            ->registerPlugin($pluginName, $pluginSrc)
            ->registerCss($pluginSrc . 'font-awesome.min.css', [
                    '_priority' => 1,
                    '_plugin'   => $pluginName,
                ], $pluginName);
    }
}