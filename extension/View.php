<?php

class View extends ViewBase
{
    const CONTENT_TYPE_PAGE     = 'PAGE';
    const CONTENT_TYPE_HTML     = 'HTML';
    const CONTENT_TYPE_JSON     = 'JSON';
    const CONTENT_TYPE_AJAXFORM = 'AJAXFORM';

    const HEADER_TYPE_PAGE      = 'text/html';
    const HEADER_TYPE_HTML      = 'text/html';
    const HEADER_TYPE_JSON      = 'application/json';
    const HEADER_TYPE_AJAXFORM  = 'text/html';

    protected $layout       = 'layout/main';
    protected $useLayout    = true;
    protected $contentType  = 'PAGE';
    protected $headerType   = 'text/html';
    protected $_rendered    = false;

    protected $title        = 'App';
    protected $_metas       = [];
    protected $_links       = [];
    protected $_styles      = [];
    protected $_scripts     = [];
    protected $_plugins     = [];

    public function init()
    {
        parent::init();

        $this->registerJs(SEA_PATH . 'extension' . DS . 'View' . DS . 'Sea.js', ['_priority' => 3], 'SeaJS');
        $this->setContentType($this->contentType);
        $this->enableRenderer();
    }

    public function setContentType($type)
    {
        switch (\HelperString::toUpper($type)) {
            case static::CONTENT_TYPE_JSON:
                $this->disableLayout();
                $this->contentType  = static::CONTENT_TYPE_JSON;
                $this->headerType   = static::HEADER_TYPE_JSON;
            break;

            case static::CONTENT_TYPE_AJAXFORM:
                $this->disableLayout();
                $this->contentType  = static::CONTENT_TYPE_AJAXFORM;
                $this->headerType   = static::HEADER_TYPE_AJAXFORM;
            break;

            case static::CONTENT_TYPE_HTML:
                $this->disableLayout();
                $this->contentType  = static::CONTENT_TYPE_HTML;
                $this->headerType   = static::HEADER_TYPE_HTML;
            break;

            case static::CONTENT_TYPE_PAGE:
            default:
                $this->enableLayout();
                $this->contentType  = static::CONTENT_TYPE_PAGE;
                $this->headerType   = static::HEADER_TYPE_PAGE;
            break;
        }

        \Sea::$app->response->setContentType($this->getHeaderType());
    }

    public function getContentType()
    {
        return $this->contentType;
    }

    public function getHeaderType()
    {
        return $this->headerType;
    }

    public function setTitle($title)
    {
        $this->title = (string) $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setLayout($view)
    {
        $view = (string) $view;
        $path = \HelperFile::path(APP_PATH . 'view/' . $view . '.php');
        if (!\HelperFile::is($path))
            throw new \ExceptionFile("Undefined layout view file '$path'");

        $this->layout = $view;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function enableLayout()
    {
        $this->useLayout = true;
    }

    public function disableLayout()
    {
        $this->useLayout = false;
    }

    public function getUseLayout()
    {
        return $this->useLayout;
    }

    public function enableRenderer()
    {
        $this->_rendered = false;
    }

    public function disableRenderer()
    {
        $this->_rendered = true;
    }

    public function getRendered()
    {
        return $this->_rendered;
    }



    public function registerMeta($params, $key = null)
    {
        if (!$params or !\HelperArray::is($params))
            return $this;

        if ($key)
            $this->_metas[$key] = $params;
        else
            $this->_metas[] = $params;

        return $this;
    }

    public function registerLink($params, $key = null)
    {
        if (!$params or !\HelperArray::is($params))
            return $this;

        if ($key)
            $this->_links[$key] = $params;
        else
            $this->_links[] = $params;

        return $this;
    }

    public function registerCss($url, $params = [], $key = null)
    {
        if (!\HelperArray::is($params))
            return $this;

        $priority = !empty($params['_priority']) ? $params['_priority'] : 99;
        unset($params['_priority']);

        $params = \HelperArray::merge(['rel' => 'stylesheet', 'type' => 'text/css', 'href' => $url], $params);
        $key = $key ?: $url;

        foreach($this->_styles as $p => $kpp) {
            foreach ($kpp as $k => $pp)
                if ($k == $key)
                    unset($this->_styles[$p][$k]);
        }

        $this->_styles[$priority][$key] = $params;

        return $this;
    }

    public function registerJs($url, $params = [], $key = null)
    {
        if (!\HelperArray::is($params))
            return $this;

        $priority = !empty($params['_priority']) ? $params['_priority'] : 99;
        unset($params['_priority']);

        $params = \HelperArray::merge(['type' => 'text/javascript', 'src' => $url], $params);
        $key = $key ?: $url;

        foreach($this->_scripts as $p => $kpp) {
            foreach ($kpp as $k => $pp)
                if ($k == $key)
                    unset($this->_scripts[$p][$k]);
        }

        $this->_scripts[$priority][$key] = $params;

        return $this;
    }

    public function getHtmlMetas()
    {
        if (!$metas = $this->_metas)
            return '';

        $lines = [];
        foreach ($metas as $params)
            $lines[] = $this->partialLib('tags/meta', ['params' => $params]);

        return implode("\n", $lines) . "\n";
    }

    public function getHtmlLinks()
    {
        if (!$links = $this->_links)
            return '';

        $lines = [];
        foreach ($links as $params)
            $lines[] = $this->partialLib('tags/link', ['params' => $params]);

        return implode("\n", $lines) . "\n";
    }

    public function getHtmlStyles()
    {
        if (!$styles = $this->_styles)
            return '';

        ksort($styles);

        $lines = [];
        foreach ($styles as $kparams) {
            foreach ($kparams as $params) {
                $destDir = !empty($params['_plugin']) ? $params['_plugin'] : null;
                unset($params['_plugin']);

                $params['href'] = $this->copyVersion($params['href'], $destDir);

                $lines[] = $this->partialLib('tags/link', ['params' => $params]);
            }
        }

        return implode("\n", $lines) . "\n";
    }

    public function getHtmlScripts()
    {
        if (!$scripts = $this->_scripts)
            return '';

        ksort($scripts);

        $lines = [];
        foreach ($scripts as $kparams) {
            foreach ($kparams as $params) {
                $destDir = !empty($params['_plugin']) ? $params['_plugin'] : null;
                unset($params['_plugin']);

                $params['src'] = $this->copyVersion($params['src'], $destDir);

                $lines[] = $this->partialLib('tags/script', ['params' => $params]);
            }
        }

        return implode("\n", $lines) . "\n";
    }

    public function registerPlugin($name, $src)
    {
        $dest = VERSIONS_PATH . $name;
        if (!\HelperDir::exists($dest))
            \HelperDir::copy($src, $dest, 0777);

        return $this;
    }

    protected function copyVersion($path, $destDir = null)
    {
        $path = \HelperString::replace('\\', '/', $path);
        if (!\HelperUrl::isAbsolute($path) and \HelperFile::exists($path)) {
            if ($destDir)
                $dest = trim($destDir, '\\/') . '/' . basename($path);
            else
                $dest = MD5($path . \HelperFile::modified($path)) . '/' . basename($path);
            $destPath = VERSIONS_PATH . $dest;
            $destWeb = VERSIONS_WWW_PATH . $dest;
            if (!\HelperFile::exists($destPath))
                \HelperFile::copy($path, $destPath);
            $path = \HelperString::replace('\\', '/', $destWeb);
        }

        return $path;
    }



    public function partial($view, $params = null)
    {
        return (new \ObjTemplate($params))->partial($view);
    }

    public function partialLib($view, $params = null)
    {
        return (new \ObjTemplate($params))->partialLib($view);
    }

    public function partialPath($path, $params = null)
    {
        return (new \ObjTemplate($params))->partialPath($path);
    }

    public function render($view, $params = null)
    {
        if ($this->getRendered())
            return;

        $this->disableRenderer();

        if ($this->contentType == static::CONTENT_TYPE_JSON or $this->contentType == static::CONTENT_TYPE_AJAXFORM) {
            if (\HelperArray::is($view))
                $params = $view;

            $content = \HelperJson::encode((array) $params);

            if ($this->contentType == static::CONTENT_TYPE_AJAXFORM)
                return \Sea::$app->content = "<script>parent.SEA_Form.ajaxSubmitCallback($content)</script>";
            else
                return \Sea::$app->content = $content;
        }

        $content = $this->partial($view, $params);

        if (!$this->useLayout)
            return \Sea::$app->content = $content;

        return \Sea::$app->content = $this->partial($this->layout, \HelperArray::merge((array) $params, [
                                                    '__title'   => $this->title,
                                                    '__metas'   => $this->getHtmlMetas(),
                                                    '__links'   => $this->getHtmlLinks(),
                                                    '__styles'  => $this->getHtmlStyles(),
                                                    '__scripts' => $this->getHtmlScripts(),
                                                    '__content' => $content,
                                                ]));

    }
}