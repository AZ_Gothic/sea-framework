<?php

class LoggerBase extends \Extension
{
    const TYPE_INFO         = 'info';
    const TYPE_WARNING      = 'warning';
    const TYPE_ERROR        = 'error';

    const FILE_MODE         = 0777;

    const LINE_SEPARATOR    = "\n";

    /**
     * Format target
     * string $name => [
     *      'path'          => string $path, required full path to log file
     *      'fileMode'      => integer $fileMode, default 0777
     *      'categories'    => string|array $categories, required
     *      'types'         => string|array $types, default '*'
     * ]
     */
    protected $targets  = [];
    protected $_logs    = [];

    protected $_defaultLogFile      = 'app.log';
    protected $_defaultLogCategory  = 'app';

    public function __construct($params = [])
    {
        $defaultCategories = [
            'app' => [
                'path'          => LOG_PATH . $this->_defaultLogFile,
                'fileMode'      => static::FILE_MODE,
                'categories'    => '*',
                'types'         => [static::TYPE_WARNING, static::TYPE_ERROR],
            ],
        ];
        $params['targets'] = !empty($params['targets']) ? $params['targets'] : [];
        $params['targets'] = \HelperArray::merge($this->normalizeTargets($defaultCategories, true),
                                                 $this->normalizeTargets($params['targets'], true));

        parent::__construct($params);
    }

    public function info($message, $category = null)
    {
        if (!$category)
            $category = $this->_defaultLogCategory;

        return $this->put($message, $category, static::TYPE_INFO);
    }

    public function warning($message, $category = null)
    {
        if (!$category)
            $category = $this->_defaultLogCategory;

        return $this->put($message, $category, static::TYPE_WARNING);
    }

    public function error($message, $category = null)
    {
        if (!$category)
            $category = $this->_defaultLogCategory;

        return $this->put($message, $category, static::TYPE_ERROR);
    }

    public function endBlock($category = null)
    {
        if (!$category)
            $category = $this->_defaultLogCategory;

        return $this->put(static::LINE_SEPARATOR, $category);
    }

    public function put($message, $category = null, $type = null)
    {
        if (!$category)
            $category = $this->_defaultLogCategory;

        if (!$type)
            $type = static::TYPE_INFO;

        if (!\HelperArray::in($type, [static::TYPE_INFO, static::TYPE_WARNING, static::TYPE_ERROR]))
            return $this;

        foreach ($this->targets as $target => $params) {
            $found = false;
            foreach($params['categories'] as $targetCategory) {
                if ($targetCategory != '*' and $targetCategory != $category)
                    continue;

                $found = true;
                break;
            }

            if (!$found)
                continue;

            $found = false;
            foreach($params['types'] as $targetType) {
                if ($targetType != '*' and $targetType != $type)
                    continue;

                $found = true;
                break;
            }

            if (!$found)
                continue;

            $this->_logs[$target][] = [
                'timestamp' => time(),
                'message'   => $message,
                'category'  => $category,
                'type'      => $type,
                'traceLine' => ($message !== static::LINE_SEPARATOR ? \HelperTrace::last() : null),
            ];
        }

        return $this;
    }

    public function toString($timestamp, $message, $category = null, $type = null, $traceLine = null)
    {
        if (!$category)
            $category = $this->_defaultLogCategory;

        if ($message === static::LINE_SEPARATOR)
            return static::LINE_SEPARATOR;

        if (!$traceLine)
            $traceLine = \HelperTrace::last();

        $str = date('Y-m-d H:i:s', $timestamp) . " [" . (string) $category . "][" . (string) $type . "]";
        if ($traceLine)
            $str .= " in '" . $traceLine['file'] . "' on line " . $traceLine['line'];
        if (\HelperArray::is($message))
            $message = implode(static::LINE_SEPARATOR, $message);
        $str .= static::LINE_SEPARATOR .(string) $message . static::LINE_SEPARATOR;

        return $str;
    }

    public function flush()
    {
        if (!$this->_logs)
            return $this;

        foreach ($this->_logs as $target => $rows) {
            $write = '';
            foreach ($rows as $row)
                $write .= $this->toString($row['timestamp'], $row['message'], $row['category'], $row['type'], $row['traceLine']);

            if ($write) {
                $file = $this->targets[$target]['path'];
                \HelperFile::put($file, $write, true, $this->targets[$target]['fileMode'], false);
            }
        }

        $this->_logs = [];
    }

    protected function normalizeTargets($targets, $throw = false)
    {
        $res = [];
        foreach ((array) $targets as $target => $params) {
            if (!\HelperString::is($target) or !\HelperArray::is($params)) {
                if ($throw)
                    throw new \ExceptionProperty("Wrong log config for '" . (string) $target . "'");
                continue;
            }

            if (empty($params['path']) or !\HelperString::is($params['path'])
                or empty($params['categories'])
                or (!\HelperString::is($params['categories']) and !\HelperArray::is($params['categories'])))
            {
                if ($throw)
                    throw new \ExceptionProperty("Wrong log config (path or categories) for '" . (string) $target . "'");
                continue;
            }

            $params['path'] = \HelperFile::path($params['path']);

            $params['categories'] = (array) $params['categories'];
            $categories = [];
            foreach ($params['categories'] as $category) {
                if ($category == '*') {
                    $categories = [$category];
                    break;
                }

                $categories[] = $category;
            }
            $params['categories'] = \HelperArray::unique($categories);

            $params['types'] = !empty($params['types']) ? (array) $params['types'] : ['*'];
            $types = [];
            foreach ($params['types'] as $type) {
                if ($type == '*') {
                    $types = [$type];
                    break;
                }

                if (\HelperArray::in($type, [static::TYPE_INFO, static::TYPE_WARNING, static::TYPE_ERROR]))
                    $types[] = $type;
            }
            if (!$types) {
                if ($throw)
                    throw new \ExceptionProperty('Wrong log config (types) for ' . (string) $target);
                continue;
            }
            $params['types'] = \HelperArray::unique($types);

            $params['fileMode'] = !empty($params['fileMode']) ? $params['fileMode'] : static::FILE_MODE;

            $res[$target] = $params;
        }

        return $res;
    }

    public function finish()
    {
        $this->flush();
    }
}