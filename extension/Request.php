<?php

class Request extends RequestBase
{
    protected $_GET         = [];
    protected $_POST        = [];
    protected $_FILES       = [];
    /* form send as pseudo ajax with files */
    protected $_isAjaxForm  = false;

    public function init()
    {
        parent::init();

        if (!empty($_GET))
            $this->_GET = $_GET;
        if (!empty($_POST))
            $this->_POST = $_POST;
        if (!empty($_FILES)) {
            $files = [];
            foreach ($_FILES as $form => $values)
                foreach ($values as $key => $formValues)
                    foreach ($formValues as $formKey => $value)
                        $files[$form][$formKey][$key] = $value;
            $this->_FILES = $files;
        }

        if ($this->get('isAjaxForm') and $this->isFiles()) {
            $this->_isAjaxForm = true;
            $this->getUnset('isAjaxForm');
        }

        if ($this->clearGlobals) {
            $_GET =
            $_POST =
            $_FILES = [];
        }
    }

    public function get($prop = null, $default = null)
    {
        return $this->params($prop, $default, 'GET');
    }

    public function getSet($prop, $value)
    {
        return $this->paramsSet($prop, $value, 'GET');
    }

    public function getUnset($prop)
    {
        return $this->paramsUnset($prop, 'GET');
    }

    public function post($prop = null, $default = null)
    {
        return $this->params($prop, $default, 'POST');
    }

    public function postSet($prop, $value)
    {
        return $this->paramsSet($prop, $value, 'POST');
    }

    public function postUnset($prop)
    {
        return $this->paramsUnset($prop, 'POST');
    }

    public function files($prop = null, $default = null)
    {
        return $this->params($prop, $default, 'FILES');
    }

    public function isAjax()
    {
        return ($this->server('HTTP_X_REQUESTED_WITH')
            and \HelperString::toLower($this->server('HTTP_X_REQUESTED_WITH')) == 'xmlhttprequest')
            or $this->isAjaxForm();
    }

    public function isGet()
    {
        return $this->getMethod() === 'GET';
    }

    public function isPost()
    {
        return $this->getMethod() === 'POST';
    }

    public function isFiles()
    {
        return $this->files() ? true : false;
    }

    public function isAjaxForm()
    {
        return $this->_isAjaxForm;
    }

    public function getMethod()
    {
        if ($this->server('HTTP_X_HTTP_METHOD_OVERRIDE'))
            return \HelperString::toUpper($this->server('HTTP_X_HTTP_METHOD_OVERRIDE'));
        if ($this->server('REQUEST_METHOD'))
            return \HelperString::toUpper($this->server('REQUEST_METHOD'));

        return 'GET';
    }
}