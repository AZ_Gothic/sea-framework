<?php

class JQuery extends \Extension
{
    protected $version = '1.11.2';

    public function __construct($params = [])
    {
        parent::__construct($params);

        \Sea::$app->view->registerJs(SEA_PATH . 'extension' . DS . 'JQuery' . DS . 'jquery-' . $this->version . '.min.js', [
                '_priority' => 1,
                '_plugin'   => 'jQuery-' .  $this->version,
            ], 'jQuery-' . $this->version);
    }
}