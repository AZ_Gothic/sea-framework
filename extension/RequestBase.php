<?php

class RequestBase extends \Extension
{
    protected $clearGlobals = true;

    protected $_SERVER      = [];

    public function init()
    {
        if (!empty($_SERVER))
            $this->_SERVER = $_SERVER;

        if ($this->clearGlobals)
            $_SERVER = [];
    }

    public function server($prop = null, $default = null)
    {
        return $this->params($prop, $default, 'SERVER');
    }

    public function params($prop = null, $default = null, $type = 'GET')
    {
        $type = \HelperString::toUpper($type);
        if ($prop === null)
            return $this->{'_' . $type};

        return isset($this->{'_' . $type}[$prop]) ? $this->{'_' . $type}[$prop] : $default;
    }

    public function paramsSet($prop, $value, $type = 'GET')
    {
        $type = \HelperString::toUpper($type);
        $this->{'_' . $type}[$prop] = $value;

        return $this;
    }

    public function paramsUnset($prop, $type = 'GET')
    {
        $type = \HelperString::toUpper($type);
        unset($this->{'_' . $type}[$prop]);

        return $this;
    }

    public function finish()
    {
        
    }
}