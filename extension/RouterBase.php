<?php

class RouterBase extends \Extension
{
    protected $_properties = [];

    public function init()
    {
        
    }

    public function set($param, $value) {
        $this->_properties[$param] = $value;

        return $this;
    }

    public function get($param = null, $default = null) {
        if (!$param)
            return $this->_properties;

        return isset($this->_properties[$param]) ? $this->_properties[$param] : $default;
    }

    public function finish()
    {
        
    }
}