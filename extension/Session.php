<?php

class Session extends \Extension
{
    protected $expired  = 86400; // 1*24*60*60 seconds, default - 1 day

    public function init()
    {
        $this->start();
    }

    protected function start()
    {
        if (!$this->isActive())
            session_start();

        if (!$this->get('_CREATED'))
            $this->set('_CREATED', time());
        elseif ((time() - $this->get('_CREATED')) > $this->expired) {
            session_regenerate_id(true);
            $this->set('_CREATED', time());
        }

        return $this;
    }

    public function isActive()
    {
        return session_status() === PHP_SESSION_ACTIVE;
    }

    public function id()
    {
        return session_id();
    }

    public function get($prop = null, $default = null)
    {
        if (!$prop)
            return isset($_SESSION) ? $_SESSION : [];
        return isset($_SESSION[$prop]) ? $_SESSION[$prop] : $default;
    }

    public function set($prop, $val)
    {
        $_SESSION[$prop] = $val;

        return $this;
    }

    public function del($prop)
    {
        unset($_SESSION[$prop]);

        return $this;
    }

    protected function close()
    {
        session_write_close();
        unset($_SESSION);

        return $this;
    }

    public function finish()
    {
        $this->close();
    }
}