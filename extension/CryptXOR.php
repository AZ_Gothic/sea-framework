<?php

class CryptXOR extends \Extension
{
    protected $key = 'secure password';

    public function encode($str)
    {
        return \HelperXOR::encode($str, $this->key);
    }

    public function decode($str)
    {
        return \HelperXOR::decode($str, $this->key);
    }
}