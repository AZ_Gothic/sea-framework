<?php

class Router extends RouterBase
{
    protected $redirectToCorrect = false;
    protected $prettyUrls        = false;
    protected $rules             = [];

    public function init()
    {
        parent::init();

        $scheme  = 'http';
        if (\Sea::$app->request->server('HTTPS', 'off') !== 'off' or \Sea::$app->request->server('SERVER_PORT') == 443)
            $scheme .= 's';
        $current = $scheme . '://' . \Sea::$app->request->server('SERVER_NAME') . (\Sea::$app->request->server('REQUEST_URI') ?: '');

        $path  = trim(\HelperUrl::parse($current, 'path'), '/');
        $route = \Sea::$app->request->get('r', '');
        if ($this->prettyUrls) {
            if ($path)
                $route = $path;
        }

        if ($route and $rules = $this->rules) {
            foreach ($rules as $r) {
                $regs   = $r['reg'];
                $reg    = \HelperArray::is($regs) ? $regs[0] : $regs;
                if (\HelperRegExp::match(new \ObjRegExp('^' . $reg . '$', 'i'), $route, $matches)) {
                    $route = $r['route'];
                    if (\HelperArray::is($regs)) {
                        foreach ($regs as $n => $param) {
                            if (!$n)
                                continue;
                            \Sea::$app->request->getSet($param, $matches[$n - 1]);
                        }
                    }
                    break;
                }
            }
        }

        $route      = trim($route, '/');
        $routeParts = explode('/', $route);
        $controller = !empty($routeParts[0]) ? \HelperString::toLower($routeParts[0]) : 'index';
        $action     = !empty($routeParts[1]) ? \HelperString::toLower($routeParts[1]) : 'index';

        $this
            ->set('current', $current)
            ->set('route', "$controller/$action")
            ->set('controller', $controller)
            ->set('action', $action)
        ;

        if ($this->redirectToCorrect)
            \Sea::$app->event->on(\sea\Controller::EVENT_BEFORE_ACTION, ['\\sea\\event\\RedirectToCorrect', 'event'], 9);
    }


    /*
        $UrlOrRoute
            empty - current url
            string - url
            array - [$controller, $action]
                or ['controller' => $controller, 'action' => $action]
                of ['controller' => $controller] or [$controller] - will set $controller/index
                of ['action' => $action] - will set $current_controller/$action
                of ['r' => $route] or ['route' => $route]
    */
    public function to($UrlOrRoute = null, $params = null, $clearOldParams = false) {
        $url    = '';
        $route  = '';
        $params = \HelperUrl::normalizeParams($params);

        if (!$UrlOrRoute) {
            $route      = $this->get('route');
            $oldParams  = \Sea::$app->request->get();
        }
        elseif (\HelperString::is($UrlOrRoute) and $UrlOrRoute == '/') {
            $route      = 'index/index';
            $oldParams  = ($route == $this->get('route')) ? \Sea::$app->request->get() : [];
        }
        elseif (\HelperString::is($UrlOrRoute) or $UrlOrRoute instanceof \ObjUrlParsed) {
            $parsed = \HelperUrl::parse($UrlOrRoute);
            if (\HelperUrl::isAbsolute($UrlOrRoute)) {
                $url        = \HelperUrl::constructUrl($parsed, true);
                $oldParams  = $parsed->query;
            }
            else {
                $oldParams  = $parsed->query;
                if ($this->prettyUrls) {
                    $route = trim($parsed->path, '/');
                    if (!$route)
                        $route = '/';
                }
                else
                    $route = !empty($oldParams['r']) ? $oldParams['r'] : '/';
            }
        }
        elseif (\HelperArray::is($UrlOrRoute)) {
            if (isset($UrlOrRoute['r']) or isset($UrlOrRoute['route'])) {
                $route = isset($UrlOrRoute['r']) ? $UrlOrRoute['r'] : $UrlOrRoute['route'];
                $parts = explode('/', trim($route, '/'));
                $UrlOrRoute = [
                    'controller' => !empty($parts[0]) ? $parts[0] : 'index',
                    'action'     => !empty($parts[1]) ? $parts[1] : 'index',
                ];
            }
            $controller = !empty($UrlOrRoute['controller'])
                        ? $UrlOrRoute['controller']
                        : (
                            !empty($UrlOrRoute[0])
                            ? $UrlOrRoute[0]
                            : $this->get('controller')
                        );
            $action = !empty($UrlOrRoute['action'])
                        ? $UrlOrRoute['action']
                        : (
                            !empty($UrlOrRoute[1])
                            ? $UrlOrRoute[1]
                            : 'index'
                        );

            $route = "$controller/$action";
            $oldParams  = ($route == $this->get('route')) ? \Sea::$app->request->get() : [];
        }
        else
            return '';

        unset($oldParams['r']);

        if ($clearOldParams)
            $oldParams = [];

        $newParams = \HelperArray::merge($oldParams, $params);

        if (!$url) {
            $url = '/';
            if ($route) {
                $route      = \HelperString::toLower($route);
                $routeClear = '';
                $routeParts = explode('/', $route);
                if (!empty($routeParts[0]) and $routeParts[0] != 'index')
                    $routeClear .= $routeParts[0];
                if (!empty($routeParts[1]) and $routeParts[1] != 'index') {
                    if (!$routeClear)
                        $routeClear = 'index';
                    $routeClear .= '/' . $routeParts[1];
                }

                if ($rules = $this->rules and \HelperArray::is($rules)) {
                    foreach ($rules as $r) {
                        if (empty($r['pattern']))
                            continue;

                        $route = trim($r['route'], '/');
                        if ($route != $routeClear)
                            continue;

                        $pattern = trim($r['pattern'], '/');
                        if (!\HelperRegExp::matchAll(new \ObjRegExp('<([^>]+)>'), $pattern, $matches)) {
                            $routeClear = $pattern;
                            break;
                        }
                        else {
                            $checked        = true;
                            $checkedParams  = [];
                            foreach ($matches as $match) {
                                if (!isset($newParams[$match])) {
                                    $checked = false;
                                    break;
                                }
                                $checkedParams[$match] = $newParams[$match];
                            }
                            if ($checked) {
                                $routeClear = $pattern;
                                foreach ($checkedParams as $param => $value) {
                                    $routeClear = \HelperString::replace("<$param>", $value, $routeClear);
                                    unset($newParams[$param]);
                                }
                            }
                        }
                    }
                }

                if ($routeClear) {
                    if ($this->prettyUrls)
                        $url .= $routeClear;
                    else
                        $newParams = \HelperArray::merge(['r' => $routeClear], $newParams);
                }
            }
        }

        return \HelperUrl::construct($url, $newParams);
    }

    public function toAbsolute($UrlOrRoute = null, $params = null, $clearOldParams = false) {
        $url = $this->to($UrlOrRoute, $params, $clearOldParams);
        if (\HelperUrl::isAbsolute($url))
            return $url;

        return rtrim(\HelperUrl::constructUrl($this->get('current'), false), '/') . '/' . ltrim($url, '/');
    }
}