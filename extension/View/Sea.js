if (typeof jQuery !== 'undefined') {
    $(document)
        .on('click', '[data-confirm="true"]', function(e) {
            e.preventDefault();

            var target = $(this);

            if (target.data('confirmed')) {
                target.data('confirmed', false);
                var href = target.attr('href');
                if (href && href !== undefined) {
                    $.ajax({
                        method:     'GET',
                        url:        href,
                        dataType:   'JSON'
                    }).done(function(response) {
                        if (response.redirect) {
                            document.location = response.redirect;
                            return false;
                        }

                        target.trigger('ajaxConfirmedResponse', [response]);
                    });
                }
            }
            else {
                var msg = target.data('confirmMessage');
                if (!msg)
                    msg = 'Are you sure?';

                if (confirm(msg))
                    target
                        .data('confirmed', true)
                        .trigger('click');
            }

            return false;
        })
        .on('input', 'FORM INPUT, FORM TEXTAREA', function(e) {
            var input = $(this);
            var fieldBlock = input.closest('.field');
            var formBlock = input.closest('FORM');
            if (fieldBlock.length) {
                fieldBlock.removeClass('has-error');
                $('.field-error', fieldBlock).remove();
            }
            if (formBlock.length) {
                $('.form-error', formBlock).remove();
                $('.form-success', formBlock).remove();
                formBlock.data('changed', true);
            }
        })
        .on('click', '.form-success, .form-error', function(e) {
            e.preventDefault();
            $(this).remove();
            return false;
        })
        .on('click', '.btn-fileInput', function(e) {
            e.preventDefault();

            var inputId = $(this).data('inputId');
            var input   = $('#' + inputId);
            if (input.length) {
                input.val('');
                input.trigger('click', [true]);
            }

            return false;
        })
        .on('submit', 'FORM[data-ajax="true"], FORM[data-ajax="1"]', function(e, ajaxSubmit) {
            if (ajaxSubmit === undefined || !ajaxSubmit) {
                var form            = $(this);
                // console.log(form.data('disabled'), form.data('changed'));
                var formDisabled    = form.data('disabled');
                if (formDisabled === undefined)
                    formDisabled    = false;
                var formChanged     = form.data('changed');
                if (formChanged === undefined)
                    formChanged     = false;

                if (formDisabled || !formChanged) {
                    e.preventDefault();
                    return false;
                }

                form.data('disabled', true);
                form.data('changed', false);
                // console.log(form.data('disabled'), form.data('changed'));

                var action  = form.data('ajaxAction');
                if (!action)
                    action  = form.attr('action');

                if (action && action !== undefined) {
                    e.preventDefault();

                    SEA_Form
                        .setForm(form)
                        .clearSuccess()
                        .clearErrors();

                    var method = form.attr('method');
                    if (!method || method === undefined)
                        method = 'GET';

                    var enctype = form.attr('enctype');
                    if (!enctype || enctype === undefined)
                        enctype = '';

                    if (enctype != 'multipart/form-data') {
                        $.ajax({
                            method:     method,
                            url:        action,
                            data:       form.serialize(),
                            dataType:   'JSON'
                        }).done(SEA_Form.ajaxSubmitCallback);
                    }
                    else {
                        $('#ajaxForm-iframe').remove();
                        $('BODY').append('<IFRAME id="ajaxForm-iframe" name="ajaxForm-iframe" class="hidden"></IFRAME>');
                        if (action.indexOf('isAjaxForm=1') == -1) {
                            action += action.indexOf('?') != -1 ? '&' : '?';
                            action += 'isAjaxForm=1';
                        }
                        form.attr('target', 'ajaxForm-iframe')
                            .attr('action', action);
                        form.trigger('submit', [true]);
                    }

                    return false;
                }
            }
        })
    ;

    var SEA_Form = {
        form: null,
        setForm: function(form) {
            var $this = SEA_Form;

            $this.form = form;

            return $this;
        },

        ajaxSubmitCallback: function(response) {
            var $this = SEA_Form;

            if (response.redirect) {
                document.location = response.redirect;
                return false;
            }

            $('#ajaxForm-iframe').remove();
            $this.form.data('disabled', false);

            if (response.errors)
                $this.constructErrors(response.errors);

            if (response.success)
                $this.constructSuccess(response.success);

            $this.form.trigger('ajaxSubmitResponse', [response]);

            return $this;
        },

        constructSuccess: function(successes) {
            var $this = SEA_Form;

            if (!$this.form)
                return $this;

            $this.clearSuccess();

            if ($.type(successes) === 'string' || $.type(successes) === 'number')
                $this.form.prepend('<div class="form-success">' + successes + '</div>');
            else
                $.each(successes, function(k, success) {
                    $this.form.prepend('<div class="form-success">' + success + '</div>');
                });

            return $this;
        },
        constructErrors: function(errors) {
            var $this = SEA_Form;

            if (!$this.form)
                return $this;

            $this.clearErrors();

            var formID = $this.form.attr('id');
            $.each(errors, function(k, error) {
                var field = $('[name="' + formID + '[' + k + ']"]', $this.form);
                if (!field.length)
                    $this.form.prepend('<div class="form-error">' + error + '</div>');
                else {
                    var fieldBlock = field.closest('.field');
                    if (fieldBlock.length) {
                        fieldBlock
                            .addClass('has-error')
                            .append('<div class="field-error">' + error + '</div>');
                    }
                }
            });

            return $this;
        },

        clearSuccess: function() {
            var $this = SEA_Form;

            if (!$this.form)
                return $this;

            $('.form-success', $this.form).remove();

            return $this;
        },
        clearErrors: function() {
            var $this = SEA_Form;

            if (!$this.form)
                return $this;

            $('.form-error', $this.form).remove();
            $('.field-error', $this.form).remove();
            $('.has-error', $this.form).removeClass('has-error');

            return $this;
        }
    };

    jQuery.fn.extend({
        insertAtCaret: function(val) {
            return this.each(function(i) {
                if (document.selection) {
                    // Internet Explorer
                    this.focus();
                    var sel     = document.selection.createRange();
                    sel.text    = val;
                    this.focus();
                }
                else if (this.selectionStart || this.selectionStart == '0') {
                    // Firefox and other Webkits
                    var startPos    = this.selectionStart;
                    var endPos      = this.selectionEnd;
                    var scrollTop   = this.scrollTop;
                    this.value      = this.value.substring(0, startPos)+val+this.value.substring(endPos,this.value.length);
                    this.focus();
                    this.selectionStart = startPos + val.length;
                    this.selectionEnd   = startPos + val.length;
                    this.scrollTop      = scrollTop;
                }
                else {
                    this.value += val;
                    this.focus();
                }
            })
        }
    });

    jQuery.getSelected = function() {
        var text = '';
        if (window.getSelection)
            text = window.getSelection().toString();
        else if (document.selection)
            text = document.selection.createRange().text;

        return text;
    }
}