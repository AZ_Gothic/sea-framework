<?php

class Response extends ResponseBase
{
    protected $headers = [];

    protected $_defaultHeaders = [
        'Content-Type' => ['Content-Type: text/html; charset=UTF-8'],
    ];

    public function __construct($params = [])
    {
        $params['headers'] = \HelperArray::merge($this->_defaultHeaders, (!empty($params['headers']) ? $params['headers'] : []));

        parent::__construct($params);
    }

    public function statusCode($code = 200)
    {
        http_response_code($code);

        return $this;
    }

    public function redirect($url, $code = 302)
    {
        if (\Sea::$app->view->contentType == \View::CONTENT_TYPE_JSON)
            \Sea::$app->view->render(['redirect' => \HelperUrl::toAbsolute($url)]);
        else
            $this->setLocation(\HelperUrl::toAbsolute($url), $code);

        $this->send();
    }

    public function setLocation($location, $code = 302)
    {
        $this
            ->statusCode($code)
            ->setHeader('Location', "Location: $location", true);

        return $this;
    }

    public function setContentType($contentType, $charset = 'UTF-8')
    {
        $this->setHeader('Content-Type', "Content-Type: $contentType; charset=$charset", true);

        return $this;
    }

    public function setHeader($type, $header, $replace = true)
    {
        if ($replace)
            $this->headers[$type] = [$header];
        else
            $this->headers[$type][] = $header;

        return $this;
    }

    public function sendHeaders()
    {
        if ($this->headers) {
            if (!empty($this->headers['Location']))
                header($this->headers['Location'][0], true);
            else {
                foreach ($this->headers as $headers) {
                    foreach ($headers as $header)
                        header($header, false);
                }
            }
        }

        $this->headers = $this->_defaultHeaders;

        return $this;
    }

    public function send($finish = true)
    {
        if ($finish) {
            if (\Sea::$app->has('view'))
                \Sea::$app->view->disableRenderer();
            \Sea::$app->finish();

            return $this;
        }

        $this->sendHeaders();

        return $this;
    }

    public function finish($finish = false)
    {
        parent::finish();

        return $this->send($finish);
    }
}