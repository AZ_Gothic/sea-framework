<?php

abstract class Extension extends \sea\Base
{
    public function __construct($params = [])
    {
        foreach ((array) $params as $param => $value) {
            $param = \HelperString::to($param);
            if (\HelperString::sub($param, 0, 1) == '_')
                throw new \ExceptionProperty('Setting protected property: ' . static::getClass($this) . "::$param");
            $this->$param = $value;
        }

        parent::__construct();
    }
}