<?php

class Event extends \Extension
{
    protected static $_events = [];

    public function on($name, $callback, $priority = 0)
    {
        if (static::isCallback($callback)) {
            if (!($callback instanceof Closure)) {
                $key = md5(\HelperJson::encode([$name, $callback]));
                static::$_events[(int) $priority][$name][$key] = $callback;
            }
            else
                static::$_events[(int) $priority][$name][] = $callback;

            ksort(static::$_events, SORT_NUMERIC);

            return true;
        }

        return false;
    }

    public function off($name, $callback = null)
    {
        if (\HelperNull::is($callback) or !($callback instanceof Closure)) {
            $key = $callback ? md5(\HelperJson::encode([$name, $callback])) : null;

            foreach (static::$_events as $priority => $named) {
                foreach ($named as $name => $data) {
                    if (!$key)
                        unset(static::$_events[$priority][$name]);
                    else
                        unset(static::$_events[$priority][$name][$key]);
                }
            }

            return true;
        }

        return false;
    }

    public function isCallback($callback, $throw = true)
    {
        if (!$res = is_callable($callback, true)) {
            if ($throw)
                throw new \ExceptionProperty('Setted property not a callback: ' . \HelperJson::encode($callback));
            return false;
        }

        return true;
    }

    public function trigger($eventName, $event = null)
    {
        if (!($event instanceof \ObjEvent)) {
            $data  = $event;
            $event = new \ObjEvent;
            $event->data = $data;
        }
        $event->name = $eventName;

        foreach (static::$_events as $priority => $named) {
            foreach ($named as $name => $events) {
                if ($eventName != $name)
                    continue;

                foreach ($events as $key => $callback) {
                    if ($this->isCallback($callback)) {
                        $event = call_user_func($callback, $event);
                        if (!($event instanceof \ObjEvent) or $event->triggered)
                            return true;
                    }
                }
            }
        }

        return false;

    }
}