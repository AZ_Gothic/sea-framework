<?php

class RouterCli extends RouterBase
{
    protected $rules = [];

    public function init()
    {
        parent::init();

        $route = \Sea::$app->request->commandRoute();

        if ($route and $rules = $this->rules) {
            foreach ($rules as $r) {
                $regs   = $r['reg'];
                $reg    = \HelperArray::is($regs) ? $regs[0] : $regs;
                if (\HelperRegExp::match(new \ObjRegExp('^' . $reg . '$', 'i'), $route, $matches)) {
                    $route = $r['route'];
                    if (\HelperArray::is($regs)) {
                        foreach ($regs as $n => $param) {
                            if (!$n)
                                continue;
                            \Sea::$app->request->commandSet($param, $matches[$n - 1]);
                        }
                    }
                    break;
                }
            }
        }

        $route      = trim($route, '/');
        $routeParts = explode('/', $route);
        $controller = !empty($routeParts[0]) ? \HelperString::toLower($routeParts[0]) : 'index';
        $action     = !empty($routeParts[1]) ? \HelperString::toLower($routeParts[1]) : 'index';

        $this
            ->set('route', "$controller/$action")
            ->set('controller', $controller)
            ->set('action', $action)
        ;
    }

    public function finish()
    {
        parent::finish();
    }
}