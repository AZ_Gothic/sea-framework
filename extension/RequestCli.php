<?php

class RequestCli extends RequestBase
{
    protected $_COMMAND_SCRIPT = null;
    protected $_COMMAND_ROUTE  = null;
    protected $_COMMAND        = [];

    public function init()
    {
        parent::init();

        if (!empty($this->_SERVER['argv'])) {
            $cmd = $this->_SERVER['argv'];

            $this->_COMMAND_SCRIPT  = !empty($cmd[0]) ? $cmd[0] : null;
            $this->_COMMAND_ROUTE   = !empty($cmd[1]) ? $cmd[1] : null;
            unset($cmd[0], $cmd[1]);

            $this->_COMMAND = !empty($cmd) ? array_values($cmd) : [];
        }

        if ($this->clearGlobals) {
            global $argv;
            $argv = $this->_SERVER['argv'] = [];
        }
    }

    public function commandScript()
    {
        return $this->_COMMAND_SCRIPT;
    }

    public function commandRoute()
    {
        return $this->_COMMAND_ROUTE;
    }

    public function command($prop = null, $default = null)
    {
        return $this->params($prop, $default, 'COMMAND');
    }

    public function commandSet($prop, $value)
    {
        return $this->paramsSet($prop, $value, 'COMMAND');
    }

    public function commandUnset($prop)
    {
        return $this->paramsUnset($prop, 'COMMAND');
    }

    public function finish()
    {
        parent::finish();
    }
}