<?php

class Config extends \Extension
{
    protected $env      = 'prod';
    protected $version  = '1.0.0';
    protected $name     = 'App';
    protected $timezone = null;
    protected $sea      = [];

    public function __construct($params = [])
    {
        $params['sea'] = require \HelperFile::path(SEA_PATH . 'config/Sea.php', true);

        parent::__construct($params);
    }

    public function get($name, $default = null)
    {
        return $this->hasProperty($name) ? $this->{$name} : $default;
    }

    public function __get($name)
    {
        return $this->get($name);
    }
}