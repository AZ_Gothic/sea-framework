<?php

namespace sea;

class Controller extends Base
{
    const EVENT_BEFORE_ACTION   = 'beforeAction';
    const EVENT_AFTER_ACTION    = 'afterAction';

    protected $view;

    public static function getControllerName($routeController)
    {
        return '\\app\\controller\\Controller' . \HelperString::toUpperFirst($routeController);
    }

    public static function getActionName($routeAction)
    {
        return 'action' . \HelperString::toUpperFirst($routeAction);
    }

    public function init()
    {
        $this->view = \Sea::$app->view;
        \Sea::$app->event->on(static::EVENT_AFTER_ACTION, ['\\sea\\event\\AutoRender', 'event'], 1);
    }

    public static function runAction($controller = null, $action = null, $return = true)
    {
        $controller = $controller ?: \Sea::$app->router->get('controller');
        $action     = $action ?: \Sea::$app->router->get('action');

        \Sea::$app->router->set('controller', $controller)
                          ->set('action', $action);

        $controller = static::getControllerName($controller);
        if (!\HelperClass::exists($controller))
            throw new \ExceptionNotFound("Undefined controller class '$controller'");

        $class = new $controller();

        $action = static::getActionName($action);

        if (!method_exists($class, $action))
            throw new \ExceptionNotFound("Undefined action method '$action'");

        $beforeAction = static::EVENT_BEFORE_ACTION;
        $class->$beforeAction();

        if ($return)
            \Sea::$app->content = $class->$action();
        else
            $class->$action();

        $afterAction = static::EVENT_AFTER_ACTION;
        $class->$afterAction();

        if ($return)
            return \Sea::$app->content;
    }

    public function beforeAction()
    {
        $event = new \ObjEventAction();
        $event->sender      = \Sea::$app->router->get('controllerName');
        $event->controller  = \Sea::$app->router->get('controller');
        $event->action      = \Sea::$app->router->get('action');

        \Sea::$app->event->trigger(static::EVENT_BEFORE_ACTION, $event);
    }

    public function afterAction()
    {
        $event = new \ObjEventAfterAction();
        $event->sender      = \Sea::$app->router->get('controllerName');
        $event->controller  = \Sea::$app->router->get('controller');
        $event->action      = \Sea::$app->router->get('action');
        $event->content     = \Sea::$app->content;

        \Sea::$app->event->trigger(static::EVENT_AFTER_ACTION, $event);
    }
}