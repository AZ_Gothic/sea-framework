<?php

namespace sea;

/*
    abstract class \sea\Base
*/
abstract class Base
{
    public function __construct() {
        if (method_exists($this, 'init'))
            call_user_func_array([$this, 'init'], func_get_args());
    }

    public function __set($name, $value) {
        $setter = 'set' . \HelperString::toUpperFirst($name);
        $getter = 'get' . \HelperString::toUpperFirst($name);
        if ($this->hasMethod($setter))
            $this->$setter($value);
        elseif ($this->hasMethod($getter))
            throw new \ExceptionProperty('Setting read-only property: ' . static::getClass($this) . "::$name");
        else
            throw new \ExceptionProperty('Setting unknown property: ' . static::getClass($this) . "::$name");
    }

    public function __get($name) {
        $setter = 'set' . \HelperString::toUpperFirst($name);
        $getter = 'get' . \HelperString::toUpperFirst($name);
        if ($this->hasMethod($getter))
            return $this->$getter();
        elseif ($this->hasMethod($setter))
            throw new \ExceptionProperty('Getting write-only property: ' . static::getClass($this) . "::$name");
        else
            throw new \ExceptionProperty('Getting unknown property: ' . static::getClass($this) . "::$name");
    }

    public function __isset($name) {
        $getter = 'get' . \HelperString::toUpperFirst($name);
        if ($this->hasMethod($getter))
            return $this->$getter() !== null;

        return false;
    }

    public function __unset($name) {
        $setter = 'set' . \HelperString::toUpperFirst($name);
        $getter = 'get' . \HelperString::toUpperFirst($name);
        if ($this->hasMethod($setter))
            $this->$setter(null);
        elseif ($this->hasMethod($getter))
            throw new \ExceptionProperty('Unsetting read-only property: ' . static::getClass($this) . "::$name");
    }

    public function __call($name, $params) {
        throw new \ExceptionMethod('Calling unknown method: ' . static::getClass($this) . "::$name()");
    }

    public static function __callStatic($name, $params) {
        throw new \ExceptionMethod('Calling unknown static method: ' . static::getCalled() . "::$name()");
    }

    protected static function getClass($object = null) {
        return get_class($object);
    }

    protected static function getCalled() {
        return get_called_class();
    }

    public function hasMethod($name) {
        if (!\HelperString::is($name))
            throw new \ExceptionMethod('hasMethod($name) expects parameter to be string, object given ' . \HelperJson::encode($name));

        return method_exists($this, $name);
    }

    public function hasProperty($name, $checkVars = true) {
        return $this->canGetProperty($name, $checkVars) or $this->canSetProperty($name, false);
    }

    public function canGetProperty($name, $checkVars = true) {
        return $this->hasMethod('get' . \HelperString::toUpperFirst($name))
                or ($checkVars and property_exists($this, $name));
    }

    public function canSetProperty($name, $checkVars = true) {
        return $this->hasMethod('set' . \HelperString::toUpperFirst($name))
                or ($checkVars and property_exists($this, $name));
    }
}