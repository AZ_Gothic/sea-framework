<?php

namespace sea;

class Extensions extends Base
{
    protected $exts;

    public function set($ext, $data) {
        if (!\HelperString::is($ext))
            throw new \ExceptionExtension('Unknown name of extension: ' . \HelperJson::encode($ext));

        if (\HelperString::is($data))
            $data = ['class' => $data];

        if (empty($data['class']))
            throw new \ExceptionExtension('Unknown class of extension: ' . $ext);

        $data['params'] = isset($data['params']) ? (array) $data['params'] : [];

        $extInstance = null;

        $class       = $data['class'];
        $extInstance = new $class($data['params']);

        if ($extInstance)
            $this->exts[$ext] = $extInstance;
    }

    public function get($ext) {
        if (!$this->has($ext))
            throw new \ExceptionExtension('Undefined extension: ' . \HelperJson::encode($ext));
        else
            return $this->exts[$ext];
    }

    public function getAll() {
        return $this->exts;
    }

    public function has($ext) {
        return !\HelperString::is($ext) ? false : isset($this->exts[$ext]);
    }

    public static function normalize($extConfig)
    {
        $res = [];
        foreach ((array) $extConfig as $ext => $data) {
            if (\HelperString::is($data))
                $data = ['class' => $data];
            $res[$ext] = $data;
        }

        return $res;
    }
}