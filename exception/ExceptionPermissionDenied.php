<?php

class ExceptionPermissionDenied extends \ExceptionInternal
{
    public function __construct($message = '', $code = 0) {
        parent::__construct($message, $code, 403);
    }

    public function getName() {
        return '403 Permission Denied';
    }
}