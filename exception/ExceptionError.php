<?php

class ExceptionError extends \ErrorException
{
    public function __construct($message = '', $code = 0, $finish = true)
    {
        parent::__construct($message, $code);

        if ($finish)
            $this->finish();
    }

    public function getName()
    {
        return 'Error Exception';
    }

    public function getRealFile()
    {
        if (!$trace = \HelperTrace::last())
            return $this->getFile();

        return $trace['file'];
    }

    public function getRealLine()
    {
        if (!$trace = \HelperTrace::last())
            return $this->getLine();

        return $trace['line'];
    }

    public function getErrorString()
    {
        return $this->getName() . ' (' . $this->getCode() . ')' . PHP_EOL . PHP_EOL
            . $this->getMessage() . PHP_EOL
            . '  in file ' . $this->getRealFile() . ' on line ' . $this->getRealLine() . PHP_EOL . PHP_EOL
            . $this->getTraceAsString() . PHP_EOL;
    }

    public function finish()
    {
        $contentType = 'text/plain';

        $error = $this->getErrorString();

        if (\Sea::$app instanceof \sea\App) {
            if (\Sea::$app->has('config')) {
                if (\Sea::$app->config->get('env') == 'prod')
                    $error = '500 Internal Server Error';
            }
            if (\Sea::$app->has('logger'))
                \Sea::$app->logger
                    ->error([$this->getMessage(), $this->getTraceAsString()])
                    ->endBlock();
            if (\Sea::$app->has('view')) {
                \Sea::$app->view->disableRenderer();
                if (\Sea::$app->view->contentType == \View::CONTENT_TYPE_JSON) {
                    $contentType = \View::HEADER_TYPE_JSON;
                    $error = \HelperJson::encode(['exception' => $error]);
                }
            }
            if (\Sea::$app->has('response')) {
                \Sea::$app->response->statusCode(500);
                \Sea::$app->response->contentType = $contentType;
            }
            else {
                http_response_code(500);
                header("Content-Type: $contentType; charset=UTF-8");
            }

            \Sea::$app->content = $error;
            \Sea::$app->finish();
        }
        elseif(\Sea::$app instanceof \sea\AppCli) {
            if (\Sea::$app->has('config')) {
                if (\Sea::$app->config->get('env') == 'prod')
                    $error = '500 Internal Server Error';
            }
            if (\Sea::$app->has('logger'))
                \Sea::$app->logger
                    ->error([$this->getMessage(), $this->getTraceAsString()])
                    ->endBlock();

            echo $error;
            \Sea::$app->finish();
        }
        else {
            header("Content-Type: $contentType; charset=UTF-8");
            echo $error;
            exit;
        }
    }
}