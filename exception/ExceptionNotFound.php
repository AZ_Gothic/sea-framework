<?php

class ExceptionNotFound extends \ExceptionInternal
{
    public function __construct($message = '', $code = 0) {
        parent::__construct($message, $code, 404);
    }

    public function getName() {
        return '404 Page Not Found';
    }
}