<?php

class ExceptionHandler
{
    protected static $_errorCodes = [
            0                   => 'UNKNOWN ERROR',
            E_ERROR             => 'E_ERROR',
            E_WARNING           => 'E_WARNING',
            E_PARSE             => 'E_PARSE',
            E_NOTICE            => 'E_NOTICE',
            E_CORE_ERROR        => 'E_CORE_ERROR',
            E_CORE_WARNING      => 'E_CORE_WARNING',
            E_COMPILE_ERROR     => 'E_COMPILE_ERROR',
            E_COMPILE_WARNING   => 'E_COMPILE_WARNING',
            E_USER_ERROR        => 'E_USER_ERROR',
            E_USER_WARNING      => 'E_USER_WARNING',
            E_USER_NOTICE       => 'E_USER_NOTICE',
            E_STRICT            => 'E_STRICT',
            E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
            E_DEPRECATED        => 'E_DEPRECATED',
            E_USER_DEPRECATED   => 'E_USER_DEPRECATED',
        ];

    protected static $_errorFatals = [
            E_ERROR,
            E_PARSE,
            E_CORE_ERROR,
            E_COMPILE_ERROR,
            E_USER_ERROR,
            E_RECOVERABLE_ERROR,
        ];

    public static function error($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno))
            return false;

        $errtype    = static::$_errorCodes[0];
        $throw      = true;
        $throwProd  = true;
        if (array_key_exists($errno, static::$_errorCodes)) {
            $errtype = static::$_errorCodes[$errno];
            if (!array_key_exists($errno, static::$_errorFatals))
                $throwProd = false;
        }

        if (\Sea::$app instanceof \sea\App) {
            if (\Sea::$app->has('config')) {
                if (\Sea::$app->config->get('env') == 'prod')
                    $throw = $throwProd;
            }
        }

        if ($throw)
            // throw new \ExceptionError("[$errtype $errno] $errstr in file '$errfile' on line $errline");
            throw new \ExceptionError("[$errtype / $errno] $errstr");

        return true;
    }
}