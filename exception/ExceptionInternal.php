<?php

class ExceptionInternal extends \ExceptionError
{
    public function __construct($message = '', $code = 0, $responseCode = 500)
    {
        parent::__construct($message, $code, false);

        $this->response($responseCode);
    }

    public function getName()
    {
        return '500 Internal Server Error';
    }

    public function response($code)
    {
        if (\Sea::$app instanceof \sea\App) {
            \Sea::$app->response->statusCode($code);

            $contentType = \Sea::$app->view->headerType;
            if (\Sea::$app->config->get('env') != 'prod') {
                $contentType = 'text/plain';
                $error       = $this->getErrorString();
            }
            else
                $error = $this->getName();

            if (\Sea::$app->view->contentType == \View::CONTENT_TYPE_JSON) {
                $contentType = \View::HEADER_TYPE_JSON;
                $error       = \HelperJson::encode(['exception' => $error]);
            }

            if (\Sea::$app->view->contentType == \View::CONTENT_TYPE_PAGE) {
                \Sea::$app->view->title  = $this->getName();
                \Sea::$app->view->layout = 'layout/error';
                $contentType             = \View::CONTENT_TYPE_PAGE;
                $error                   = \Sea::$app->view->render('error', ['error' => ((\Sea::$app->config->get('env') != 'prod') ? $error: '')]);
            }

            \Sea::$app->view->disableRenderer();
            \Sea::$app->content = $error;
            \Sea::$app->response->contentType = $contentType;
        }
        elseif(\Sea::$app instanceof \sea\AppCli) {
            if (\Sea::$app->config->get('env') != 'prod')
                echo $this->getErrorString();
            else
                echo $this->getName();
        }

        \Sea::$app->finish();
    }
}