<?php
    $field    = $this->form['field'];
    $value    = $this->form['value'];
    $hasError = $this->form['hasError'];
    $error    = $this->form['error'];
?>
<div class="field<?= $hasError ? ' has-error' : '' ?>">
    <textarea<?php foreach($field as $a => $av) { echo ' ' . $a . '="' . \HelperString::toAttr($av) . '"'; } ?>><?= $value ?></textarea>
    <?php if ($hasError) : ?>
        <div class="field-error"><?= \HelperString::html($error) ?></div>
    <?php endif; ?>
</div>