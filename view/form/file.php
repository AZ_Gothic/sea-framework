<?php
    $field    = $this->form['field'];
    $field['class'] = (!empty($field['class']) ? $field['class'] . ' ' : '') . 'hidden';
    $button   = !empty($field['button']) ? $field['button'] : 'File';
    $hasError = $this->form['hasError'];
    $error    = $this->form['error'];
?>
<div class="field<?= $hasError ? ' has-error' : '' ?>">
    <input<?php foreach($field as $a => $av) { echo ' ' . $a . '="' . \HelperString::toAttr($av) . '"'; } ?> value=""/>
    <a href="#" class="btn btn-fileInput" data-input-id="<?= $field['id'] ?>"><?= $button ?></a>
    <?php if ($hasError) : ?>
        <div class="field-error"><?= \HelperString::html($error) ?></div>
    <?php endif; ?>
</div>