<?php

class ValidatorNotEmpty implements \InterfaceValidator
{
    public static $message = 'Value cannot be empty';

    public static function validate($value, $params = null, $message = null)
    {
        $return = new \ObjValidate;
        $return->result  = !empty($value);
        $return->message = $message ?: static::$message;

        return $return;
    }
}