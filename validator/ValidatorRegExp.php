<?php

class ValidatorRegExp implements \InterfaceValidator
{
    public static $message = 'Wrong value';
    public static $pattern = '~.*~';

    public static function validate($value, $params = null, $message = null)
    {
        if (!$params)
            $params = static::$pattern;

        $return = new \ObjValidate;
        $return->result  = \HelperRegExp::match($params, $value);
        $return->message = $message ?: static::$message;

        return $return;
    }
}