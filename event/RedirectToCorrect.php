<?php

namespace sea\event;

class RedirectToCorrect implements \InterfaceEvent
{
    public static function event(\ObjEvent $event)
    {
        if (\Sea::$app->request->isGet() and \Sea::$app->router->get('current') != \Sea::$app->router->toAbsolute()) {
            $event->triggered = get_called_class();
            \Sea::$app->response->redirect(null, 301);
        }

        return $event;
    }
}