<?php

namespace sea\event;

class AutoRender implements \InterfaceEvent
{
    public static function event(\ObjEvent $event)
    {
        if (!\Sea::$app->view->getRendered())
            \Sea::$app->content = \Sea::$app->view->render(
                \Sea::$app->router->get('controller') . '/' . \Sea::$app->router->get('action'),
                (array) $event->content
            );

        return $event;
    }
}